const path = require("path"),
  manifest = require("../manifest"),
  HtmlWebpackPlugin = require("html-webpack-plugin");

const pages = {
  "index": {
    title: process.env.SITE_NAME,
    description: process.env.SITE_DESCRIPTION,
    domain: process.env.DOMAIN,
    thumbnail: process.env.THUMBNAIL
  },
};

module.exports = Object.keys(pages).map((filename) => {
  const page = pages[filename];
  return new HtmlWebpackPlugin({
    template: path.join(manifest.paths.input, `${filename}.html`),
    path: manifest.paths.output,
    filename: `${filename}.html`,
    inject: true,
    hash: true,
    minify: {
      collapseWhitespace: manifest.IS_PRODUCTION,
      minifyCSS: manifest.IS_PRODUCTION,
      minifyJS: manifest.IS_PRODUCTION,
      removeComments: manifest.IS_PRODUCTION,
      useShortDoctype: manifest.IS_PRODUCTION,
    },
    title: `${page.title}`,
    API_URL: process.env.API_URL,
    GA_ID: process.env.GA_ID,
    GA_EVENT_CATEGORY:process.env.GA_EVENT_CATEGORY,
    meta: {
      "viewport": `width=device-width,initial-scale=1`,
      "title": `${page.title}`,
      "description": `${page.description}`,

      "og:type": "Website",
      "og:url": `${page.domain}`,
      "og:title": `${page.title}`,
      "og:description": `${page.description}`,
      "og:image": `${page.domain}/${page.thumbnail}`,

      "twitter:card": "summary_large_image",
      "twitter:url": `${page.domain}`,
      "twitter:title": `${page.title}`,
      "twitter:description": `${page.description}`,
      "twitter:image": `${page.domain}/${page.thumbnail}`,
    }
  });
});
