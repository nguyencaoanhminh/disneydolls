// ---------------------------------------
// @Copy files from src to build
// ---------------------------------------
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");
const manifest = require("../manifest");

const patterns = [
  {
    from: path.resolve(manifest.paths.input, `assets/images`),
    to: path.resolve(manifest.paths.output, `images`),
  },
  {
    from: path.resolve(manifest.paths.input, `assets/videos`),
    to: path.resolve(manifest.paths.output, `videos`),
  },
];

if (process.env.THUMBNAIL) {
  patterns.push({
    from: path.resolve(manifest.paths.input, `assets/${process.env.THUMBNAIL}`),
    to: path.resolve(manifest.paths.output, `${process.env.THUMBNAIL}`),
  });
}

if (process.env.MINIGAME && process.env.MINIGAME === 'TRUE') {
  patterns.push({
    from: path.resolve(manifest.paths.input, `minigame`),
    to: path.resolve(manifest.paths.output, `minigame`),
  });
}

module.exports = new CopyWebpackPlugin({
  patterns: patterns,
});
