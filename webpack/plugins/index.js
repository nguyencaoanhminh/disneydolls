const manifest = require('../manifest');
const plugins = [];
plugins.push(
    require('./dotenv'),
    require('./cleanWebpackPlugin'),
    require('./miniCssExtractPlugin'),
    ...(require('./htmlPlugin')),
    require('./jquery'),
    require('./copyWebpackPlugin'),
    // require('./nodemonPlugin')
);
module.exports = plugins;