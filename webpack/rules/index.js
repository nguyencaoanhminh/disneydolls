module.exports = [
    require('./js'),
    require('./sass'),
    require('./fonts'),
    require('./images'),
    require('./videos'),
    require('./doc'),
    require('./html'),
    require('./ts')
];