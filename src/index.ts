import "jquery";
import "bootstrap";
import "owl.carousel";

import "./index.scss";
import LEGOFriendsMicrosite from "./main";

import { LEGO_FRIENDS_MICROSITE_CONFIG } from "./config";

if (process.env.NODE_ENV == "development") {
  console.log("Current version: " + require("../package.json").version);
}

const config = LEGO_FRIENDS_MICROSITE_CONFIG;
window.Microsite = new LEGOFriendsMicrosite(config);
