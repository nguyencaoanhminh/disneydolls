import { PlaylistCarouselConfig } from "./../../interfaces/playlist";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_right.png";
import Playlist from "./playlist";
import { LayoutUtils, TrackingUtils } from "@core/utils";

export const DEFAULT_PLAYLIST_CAROUSEL_CONFIG: PlaylistCarouselConfig = {
  selector: "",
  loop: true,
  margin: 15,
  // autoplay: true,
  // autoplayTimeout: 5000,
  nav: true,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    768: {
      items: 3,
    },
    993: {
      items: 3.5,
    },
  },
};

export default class PlaylistCarousel {
  config: PlaylistCarouselConfig;
  playlist!: Playlist;
  element!: HTMLElement | null;

  constructor(config: PlaylistCarouselConfig, playlist: Playlist) {
    this.config = {
      ...DEFAULT_PLAYLIST_CAROUSEL_CONFIG,
      ...config,
    };
    this.playlist = playlist;
    this.init();
  }

  init() {
    this.initElements();
    this.initCarousel();
  }

  initElements() {
    this.element = document.querySelector(this.config.selector);
  }

  initCarousel() {
    if (this.element) {
      const config = {
        ...this.config,
        ...{
          onInitialized: this.onInitialized.bind(this),
          onChanged: this.onChanged.bind(this),
        },
      };
      LayoutUtils.windowOnLoad(() => {
        $(this.element as HTMLDivElement).owlCarousel(config);
      });
    }
  }

  onInitialized(event: any) {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }

    if (this.playlist.tracking) {
      const $carousel = $(this.element as HTMLDivElement);
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      const $items = $($carousel.find(".section-videos-carousel-item"));
      $prev.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Left Click";
        if (this.playlist.key == "products-carousel") {
          TrackingUtils.SendGA("Left Button Click", "Product Gallery", "Engagement");
        } else {
          // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
          TrackingUtils.SendGA(`${carouselName} ${eventName}`, carouselName, "Video Interaction");
        }
      });

      $next.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Right Click";
        if (this.playlist.key == "products-carousel") {
          TrackingUtils.SendGA("Right Button Click", "Product Gallery", "Engagement");
        } else {
          // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
          TrackingUtils.SendGA(`${carouselName} ${eventName}`, carouselName, "Video Interaction");
        }
      });

      $items.each((index: number, item: HTMLElement) => {
        $(item).on("click", (e) => {
          const name = $(item).data("name") ?? "Unknown";
          const eventName = "Click";
          TrackingUtils.SendGA(`${name} ${eventName}`, "TV Series Episode", "Video Interaction");
        });
      });
    }
  }

  onChanged() {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }
  }
}
