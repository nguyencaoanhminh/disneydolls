import PlaylistCarousel, { DEFAULT_PLAYLIST_CAROUSEL_CONFIG } from "./playlist-carousel";
import { PlaylistConfig } from "./../../interfaces/playlist";

export const DEFAULT_PLAYLIST_CONFIG: PlaylistConfig = {
  key: "playlist",
  tracking: false,
  carousel: DEFAULT_PLAYLIST_CAROUSEL_CONFIG,
};

export default class Playlist {
  key: string;
  tracking: boolean;
  config: PlaylistConfig;
  microsite;
  carousel!: PlaylistCarousel;
  constructor(config: PlaylistConfig) {
    this.config = config;
    this.key = this.config.key;
    this.tracking = !!this.config.tracking;
    this.microsite = window.Microsite;
    if (config.carousel && !(config.carousel instanceof Array)) {
      this.config.carousel = {
        ...DEFAULT_PLAYLIST_CAROUSEL_CONFIG,
        ...config.carousel,
      };
    }

    this.init();
  }

  init() {
    this.initCarousel();
  }

  initCarousel() {
    if(this.config.carousel) {
      this.carousel = new PlaylistCarousel(this.config.carousel, this);
    }
  }
}
