import { VideoSlider } from "@core/modules/video-slider";
import { VideoSliderVideoConfig } from "@core/interfaces/video-slider";

export const DEFAULT_VIDEO_SLIDER_VIDEO_CONFIG: VideoSliderVideoConfig = {
  selector: "",
};
export default class VideoSliderVideo {
  slider;
  config;
  element!: HTMLVideoElement;
  constructor(config: VideoSliderVideoConfig, slider: VideoSlider) {
    this.config = { ...DEFAULT_VIDEO_SLIDER_VIDEO_CONFIG, ...config };
    this.slider = slider;
    this.init();
  }

  init() {
    this.initElement();
  }

  initElement() {
    if (this.config.selector) {
      this.element = document.querySelector(this.config.selector) as HTMLVideoElement;
    }
  }
}
