import { VideoSliderConfig } from "./../../interfaces/video-slider";
import VideoSliderCarousel, { DEFAULT_VIDEO_SLIDER_CAROUSEL_CONFIG } from "./video-slider-carousel";
import VideoSliderVideo, { DEFAULT_VIDEO_SLIDER_VIDEO_CONFIG } from "./video-slider-video";

const DEFAULT_VIDEO_SLIDER_CONFIG: VideoSliderConfig = {
  tracking: true,
  key: "",
};

export default class VideoSlider {
  key: string;
  tracking: boolean;
  config: VideoSliderConfig;
  microsite;
  video!: VideoSliderVideo;
  carousel!: VideoSliderCarousel;
  constructor(config: VideoSliderConfig) {
    this.config = { ...DEFAULT_VIDEO_SLIDER_CONFIG, ...config };
    this.key = this.config.key;
    this.tracking = !!this.config.tracking;
    this.microsite = window.Microsite;
    if (config.carousel && config.carousel instanceof Object) {
      this.config.carousel = {
        ...DEFAULT_VIDEO_SLIDER_CAROUSEL_CONFIG,
        ...config.carousel,
      };
    }

    if (config.video && config.carousel instanceof Object) {
      this.config.video = {
        ...DEFAULT_VIDEO_SLIDER_VIDEO_CONFIG,
        ...config.video,
      };
    }

    this.init();
    // console.log(this);
  }

  init() {
    this.initVideo();
    this.initCarousel();
  }

  initVideo() {
    if(this.config.video) {
      this.video = new VideoSliderVideo(this.config.video, this);
    }
  }

  initCarousel() {
    if(this.config.carousel) {
      this.carousel = new VideoSliderCarousel(this.config.carousel, this);
    }
  }
}
