import { TrackingUtils } from "@core/utils";
import { LayoutUtils } from "@core/utils";
import { VideoCompletion, VideoSliderCarouselConfig } from "./../../interfaces/video-slider";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_right.png";
import VideoSlider from "./video-slider";

export const DEFAULT_VIDEO_SLIDER_CAROUSEL_CONFIG: VideoSliderCarouselConfig = {
  selector: "",
  itemSelector: ".video-slider-item",
  loop: false,
  margin: 15,
  autoplay: false,
  nav: false,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    // 768: {
    //   margin: 15,
    //   items: 3,
    // },
    // 993: {
    //   margin: 81,
    //   items: 3,
    // },
  },
};

export default class VideoSliderCarousel {
  slider: VideoSlider;
  element!: HTMLDivElement;
  titleEl!: HTMLElement;
  config: VideoSliderCarouselConfig;

  get video() {
    return this.slider.video;
  }

  get videoEl() {
    return this.slider.video.element;
  }

  get carouselEl() {
    return this.element;
  }

  constructor(config: VideoSliderCarouselConfig, slider: VideoSlider) {
    this.slider = slider;
    this.config = { ...DEFAULT_VIDEO_SLIDER_CAROUSEL_CONFIG, ...config };
    this.init();
  }

  init() {
    this.initElements();
    this.initCarousel();
  }

  initElements() {
    this.element = document.querySelector(this.config.selector) as HTMLDivElement;
    if (this.config.titleSelector) {
      this.titleEl = document.querySelector(this.config.titleSelector) as HTMLElement;
    }
  }

  initCarousel() {
    if (this.element) {
      const config = {
        ...this.config,
        ...{
          onInitialized: this.onInitialized.bind(this),
          onChanged: this.onChanged.bind(this),
        },
      };
      LayoutUtils.windowOnLoad(() => {
        $(this.element as HTMLDivElement).owlCarousel(config);
      });
    }
  }

  onInitialized(event: any) {
    // console.log(this);
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }

    const $carousel = $(this.element);
    const $items = $($carousel.find(this.config.itemSelector || ".video-slider-item"));

    $items.on("click", this.onClickItem.bind(this));

    if (this.slider.tracking) {
      const $carousel = $(this.element as HTMLDivElement);
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      $prev.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Left Click";
        // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
      });

      $next.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Right Click";
        // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
      });
    }
  }

  onChanged() {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }
  }

  onClickItem(event: Event) {
    event.stopPropagation();

    const item = event.target as HTMLElement;

    // console.log(item);
    /** Save current state for video target */
    const currentIndex = this.videoEl.getAttribute("data-index") || "0";
    const currentEls: NodeListOf<HTMLElement> = this.carouselEl.querySelectorAll(`.video-slider-item[data-index="${currentIndex}"]`);
    const currentTime = this.videoEl.currentTime == this.videoEl.duration ? 0 : this.videoEl.currentTime;
    const currentCompletion = this.getVideoCompletion(this.videoEl);

    currentEls.forEach((currentEl) => {
      currentEl.setAttribute("data-index", currentIndex);
      currentEl.setAttribute("data-time", currentTime.toString());
      this.setVideoCompletion(currentEl, currentCompletion);
    });

    /** Set new attribute for video target */
    const src = item.getAttribute("data-src");
    const index = item.getAttribute("data-index") ?? 0;
    const time = item.getAttribute("data-time") ?? 0;
    const name = item.getAttribute("data-name") ?? "";
    const completion = this.getVideoCompletion(item);

    this.carouselEl.querySelectorAll(".video-slider-item").forEach((el) => el.classList.remove("active"));
    this.carouselEl.querySelectorAll(`.video-slider-item[data-index="${index}"]`).forEach((el) => el.classList.add("active"));

    this.videoEl.setAttribute("data-index", index.toString());
    this.videoEl.setAttribute("data-name", name);
    this.setVideoCompletion(this.videoEl, completion);

    this.videoEl.style.height = this.videoEl.getBoundingClientRect().height + "px";
    this.videoEl.style.width = this.videoEl.getBoundingClientRect().width + "px";
    this.videoEl.src = `${src}#t=${time}`;
    this.videoEl.removeAttribute("poster");
    this.videoEl.play();

    if (this.titleEl) {
      const title = item.getAttribute("data-title") || "";
      this.titleEl.innerHTML = title;
    }

    if (this.slider.tracking) {
      const name = $(item).data("name") ?? "Unknown";
      const eventName = "Click";
      // TrackingUtils.SendGA(`${name} ${eventName}`);
    }
  }

  getVideoCompletion(element: HTMLElement): VideoCompletion {
    return [25, 50, 75, 100].map((percent) => {
      return {
        percent: percent,
        status: element.getAttribute(`data-${percent}`) ?? "false",
      };
    });
  }

  setVideoCompletion(element: HTMLElement, completion: VideoCompletion) {
    completion.forEach((data) => {
      element.setAttribute(`data-${data.percent}`, data.status.toString());
    });
  }
}
