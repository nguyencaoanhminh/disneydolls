import { LayoutUtils } from "./../../utils/layout";
import { LoaderConfig } from "./../../interfaces/layout";
import { Layout } from ".";

export const DEFAULT_LOADER_CONFIG: LoaderConfig = {
  selector: "#loader",
  timeout: 1000,
};

export default class Loader {
  config: LoaderConfig;
  layout: Layout;

  element!: HTMLElement;
  isHide: boolean = false;

  constructor(config: boolean | LoaderConfig, layout: Layout) {
    if (typeof config === "boolean") {
      this.config = DEFAULT_LOADER_CONFIG;
    } else {
      this.config = { ...DEFAULT_LOADER_CONFIG, ...config };
    }
    this.layout = layout;

    this.initElement();
    this.initEvent();
  }

  initElement() {
    this.element = document.querySelector(this.config.selector) as HTMLElement;
  }

  initEvent() {
    LayoutUtils.windowOnLoad(() => {
      this.hide();
    }, this.config.timeout);
  }

  hide() {
    this.element.classList.add("hiding");
    document.body.classList.remove("loading");
    this.isHide = true;
    setTimeout(() => {
      this.element.classList.add("hide");
    }, 1000);
  }
}
