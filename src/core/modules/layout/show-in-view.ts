import { ShowInViewConfig } from "./../../interfaces/layout";
import { Layout } from ".";

export const DEFAULT_SHOW_IN_VIEW_CONFIG: ShowInViewConfig = {
  selector: '[data-showInView="true"]',
};

export default class ShowInView {
  config: ShowInViewConfig;
  layout: Layout;
  constructor(config: boolean | ShowInViewConfig, layout: Layout) {
    if (typeof config === "boolean") {
      this.config = DEFAULT_SHOW_IN_VIEW_CONFIG;
    } else {
      this.config = { ...DEFAULT_SHOW_IN_VIEW_CONFIG, ...config };
    }
    this.layout = layout;

    this.init();
  }

  init() {
    this.loop();
  }

  scroll(callback: () => void) {
    window.setTimeout(callback.bind(this), 1000 / 60);
  }

  itemInViewPort(item: Element, distance = 50) {
    let rect = item.getBoundingClientRect();
    return rect.top <= (window.innerHeight - distance || document.documentElement.clientHeight - distance);
  }

  timeOutMeet(item: any, time: number) {
    setTimeout(() => {
      item.removeClass("none");
      item.addClass("active");
    }, time);
  }

  loop() {
    let paisley = $(".section-meet-list-container-item .doodles.paisley");
    let autumn = $(".section-meet-list-container-item .doodles.autumn");
    let liann = $(".section-meet-list-container-item .doodles.liann");
    let olly = $(".section-meet-list-container-item .doodles.olly");
    let aliya = $(".section-meet-list-container-item .doodles.aliya");
    let leo = $(".section-meet-list-container-item .doodles.leo");
    let zac = $(".section-meet-list-container-item .doodles.zac");
    let nova = $(".section-meet-list-container-item .doodles.nova");
    let viewMeet = window.innerWidth > 575 ? 300 : 150;

    if (!this.layout.loader || (this.layout.loader && this.layout.loader.isHide)) {
      document.querySelectorAll('[data-showInView="false"]').forEach((item: Element) => {
        if (!item.getAttribute("data-preventShowInView") && this.itemInViewPort(item)) {
          item.setAttribute("data-showInView", "true");
          item.addEventListener("webkitAnimationEnd", () => {
            item.setAttribute("data-disabledAnimation", "true");
          });
        }
      });
      document.querySelectorAll('[data-showWaveInView="false"]').forEach((item: Element) => {
        if (!item.getAttribute("data-preventShowInView") && this.itemInViewPort(item, viewMeet)) {
          item.setAttribute("data-showWaveInView", "true");
          item.classList.add("active");
          this.timeOutMeet(paisley, 200);
          this.timeOutMeet(autumn, 500);
          this.timeOutMeet(liann, 800);
          this.timeOutMeet(olly, 1100);
          this.timeOutMeet(aliya, 1400);
          this.timeOutMeet(leo, 1700);
          this.timeOutMeet(zac, 2000);
          this.timeOutMeet(nova, 2300);

          item.addEventListener("webkitAnimationEnd", () => {
            item.setAttribute("data-disabledAnimation", "true");
          });
        }
      });

      document.querySelectorAll('[data-showScrollDownInView="false"]').forEach((item: Element) => {
        if (!item.getAttribute("data-preventShowInView") && this.itemInViewPort(item)) {
          item.setAttribute("data-showScrollDownInView", "true");

          item.addEventListener("webkitAnimationEnd", () => {
            item.setAttribute("data-disabledAnimation", "true");
          });
        }
      });
    }

    this.scroll(this.loop.bind(this));
  }
}
