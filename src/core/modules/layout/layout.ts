import Microsite from "@core/index";
import { LayoutUtils, TrackingUtils } from "@core/utils";
import { LayoutConfig } from "../../interfaces/layout";
import Loader from "./loader";
import ShowInView from "./show-in-view";

export const DEFAULT_LAYOUT_CONFIG: LayoutConfig = {
  showInView: false,
  autoScrollTop: true,
  loader: false,
  autoStopVideo: false,
};

export default class Layout {
  config: LayoutConfig;
  microsite: Microsite;

  loader!: Loader;
  showInView!: ShowInView;

  constructor(config: LayoutConfig, microsite: Microsite) {
    this.microsite = microsite;
    this.config = { ...DEFAULT_LAYOUT_CONFIG, ...config };
    this.init();
  }

  init() {
    this.initLoader();
    this.initShowInView();
    this.initAutoScrollTop();
    this.initGame();
    this.closeGame();
    this.initWM();
  }

  initLoader() {
    if (this.config.loader) {
      this.loader = new Loader(this.config.loader, this);
    }
  }

  initShowInView() {
    if (this.config.showInView) {
      this.showInView = new ShowInView(this.config.showInView, this);
    }
  }

  initAutoScrollTop() {
    if (this.config.autoScrollTop) {
      LayoutUtils.windowOnLoad(this.scrollToTop.bind(this));
    }
  }

  scrollToTop() {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }

  initWM() {
    $(".section-videos-button img").on("click", () => {
      TrackingUtils.SendGA("TV Series Episode Watch More Click", "TV Series Episode", "Video Interaction");
    })
  }
  
  initGame() {
    const gamePopup = $(".popup-game");
    const gameIframe: any = gamePopup.find("iframe");
    $(".game__card, .game__card-rule").on("click", () => {
      TrackingUtils.SendGA("'Play Now' Click", "Mini Game", "Engagement");
      $('iframe').attr('src', 'minigame/index.html');
      gamePopup.addClass("show");
      $("body").addClass("popup");
      $("video").trigger("pause");

      if (gameIframe[0].contentWindow.TATracker) {
        let interval = setInterval(() => {
          gameIframe[0].contentWindow?.TATracker?.updateDwellTime();
        }, 5000);
        if ($("body").hasClass("popup")) {
          interval;
        } else {
          clearInterval(interval);
        }
      }  
      let htmlTags: any = document.getElementsByTagName("html")
      for(var i=0; i < htmlTags.length; i++) {
          htmlTags[i].style.overflowY = "hidden";
      }
    });
  
    gameIframe.attr("src", gameIframe.attr("data"));
  }
  closeGame() {
    $(".popup-close").on("click", () => {
      const gamePopup: any = $(".popup-game");
      const gameIframe = gamePopup.find("iframe");
      $('iframe').removeAttr('src');
      $("body").removeClass("popup");
      $(".popup-game").removeClass("show");
      if (gameIframe[0].contentWindow.TATracker) {
        gameIframe[0].contentWindow.TATracker._dwell = 0;
      }  
      // enableScroll();
      let htmlTags: any = document.getElementsByTagName("html")
      for(var i=0; i < htmlTags.length; i++) {
          htmlTags[i].style.overflowY = "auto";
      }
    });
    $('iframe').on("click", () => {
      console.log('tre')
    })
  }
}
