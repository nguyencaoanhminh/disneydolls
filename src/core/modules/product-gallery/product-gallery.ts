import { ProductGalleryConfig } from "./../../interfaces/product-gallery";
import { GalleryConfig } from "./../../interfaces/gallery";
import { ProductGalleryModal } from "./product-gallery-modal";
import { ProductGalleryToggle } from "./product-gallery-toggle";
import { ProductGalleryCarousel } from "./product-gallery-carousel";

const DEFAULT_PRODUCT_GALLERY_CONFIG: ProductGalleryConfig = {
  key: "product-gallery",
  tracking: false,
};

export default class ProductGallery {
  key: string;
  tracking: boolean = false;
  config;
  microsite;
  modal!: ProductGalleryModal;
  toggle!: ProductGalleryToggle;
  carousel!: ProductGalleryCarousel;

  constructor(config: ProductGalleryConfig) {
    this.config = { ...DEFAULT_PRODUCT_GALLERY_CONFIG, ...config };
    this.key = this.config.key;
    this.tracking = !!this.config.tracking;
    this.microsite = window.Microsite;
    this.init();
  }

  init() {
    if (this.config.modal) {
      this.modal = new ProductGalleryModal(this.config.modal, this);
    }
    if (this.config.toggle && !this.config.carousel) {
      this.toggle = new ProductGalleryToggle(this.config.toggle, this);
    }
    if (this.config.carousel) {
      this.carousel = new ProductGalleryCarousel(this.config.carousel, this);
    }
  }
}
