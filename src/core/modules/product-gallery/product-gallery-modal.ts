import { TrackingUtils } from "./../../utils/tracking";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_right.png";
import { ProductGalleryModalConfig } from "../../interfaces/product-gallery";
import ProductGallery from "./product-gallery";

const DEFAULT_PRODUCT_GALLERY_MODAL_CAROUSEL_CONFIG = {
  loop: true,
  items: 1,
  singleItem: true,
  margin: 30,
  nav: false,
  dots: false,
  mouseDrag: false,
  touchDrag: false,
  pullDrag: false,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
};

export const DEFAULT_PRODUCT_GALLERY_MODAL_CONFIG = {
  selector: "",
  data: [],
  carousel: DEFAULT_PRODUCT_GALLERY_MODAL_CAROUSEL_CONFIG,
};

export class ProductGalleryModal {
  gallery: ProductGallery;
  config: ProductGalleryModalConfig;
  modalEl!: HTMLDivElement | null;
  btnCloseEl!: HTMLButtonElement | null;
  contentEl!: HTMLDivElement | null;
  carouselEl!: HTMLDivElement | null;
  activeGalleryId!: string;
  constructor(config: ProductGalleryModalConfig, gallery: ProductGallery) {
    this.config = config;
    this.gallery = gallery;
    this.initElements();
  }

  initElements() {
    this.modalEl = document.querySelector(this.config.selector);
    if (this.modalEl) {
      this.btnCloseEl = this.modalEl.querySelector(".modal-product-gallery-close");
      this.contentEl = this.modalEl.querySelector(".modal-product-gallery-body-inner");
      this.carouselEl = this.modalEl.querySelector(".modal-product-gallery-carousel");
      this.initEvents();
      this.initCarousel();
    }
  }

  initCarousel() {
    if (!this.carouselEl) return;
    const html = this.config.data
      .map((data, index) => {
        return `
        <div class="item" data-product_gallery_id="${data.id}">
          <div class="modal-product-gallery-carousel-item-inner">
            <div class="modal-product-gallery-left">
                <div class="modal-product-gallery-image-container">
                  <img src="images/${data.image}" data-index="${index}" class="modal-product-gallery-image" alt="Product Gallery ${data.id} - Image" />
                </div>
            </div>
            <div class="modal-product-gallery-right">
              <div class="modal-product-gallery-infor">
                <h3 class="modal-product-gallery-infor-title">${data.title} (${data.id})</h3>
                <p class="modal-product-gallery-infor-desc">${data.description}</p>
              </div>
            </div>
          </div>
        </div>
      `;
      })
      .join("\n");
    this.carouselEl.innerHTML = html;
    const config = {
      ...DEFAULT_PRODUCT_GALLERY_MODAL_CAROUSEL_CONFIG,
      ...{
        onInitialized: this.onCarouselInitialized.bind(this),
        onChanged: this.onCarouselChanged.bind(this),
      },
      ...this.config.carousel,
    };
    $(this.carouselEl).owlCarousel(config);
  }

  initEvents() {
    this.btnCloseEl?.addEventListener("click", (e) => {
      this.hide();
      if (this.gallery.tracking) {
        const modalName = this.modalEl?.getAttribute("data-name");
        const eventName = "Close Click";
        // TrackingUtils.SendGA(`${modalName} ${eventName}`);
      }
    });
  }

  show(galleryId: string, index = 0) {
    this.activeGalleryId = galleryId;
    this.updateModal();
    $(this.carouselEl as HTMLDivElement).trigger("to.owl.carousel", index);
    $(this.modalEl as HTMLDivElement).modal("show");
  }

  hide() {
    $(this.modalEl as HTMLDivElement).modal("hide");
  }

  updateModal() {
    this.modalEl?.setAttribute("data-product_gallery_id", this.activeGalleryId);
    if (this.gallery.tracking) {
      this.modalEl?.setAttribute("data-name", `Playset ${this.activeGalleryId}`);
    }
  }

  onCarouselInitialized() {
    const $carousel = $(this.carouselEl as HTMLDivElement);
    if (this.config.carousel?.nav && $($carousel.find(".owl-nav")).hasClass("disabled")) {
      $($carousel.find(".owl-nav")).removeClass("disabled");
    }

    if (this.gallery.tracking) {
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      $prev.on("click", (e) => {
        const modalName = this.modalEl?.getAttribute("data-name");
        const carouselName = this.carouselEl?.getAttribute("data-name");
        const eventName = "Left Click";
        // TrackingUtils.SendGA(`${modalName} ${carouselName} ${eventName}`);
      });

      $next.on("click", (e) => {
        const modalName = this.modalEl?.getAttribute("data-name");
        const carouselName = this.carouselEl?.getAttribute("data-name");
        const eventName = "Right Click";
        // TrackingUtils.SendGA(`${modalName} ${carouselName} ${eventName}`);
      });
    }
  }

  onCarouselChanged(e: any) {
    const $carousel = $(this.carouselEl as HTMLDivElement);
    if (this.config.carousel?.nav && $($carousel.find(".owl-nav")).hasClass("disabled")) {
      $($carousel.find(".owl-nav")).removeClass("disabled");
    }

    const currentItemEl = this.carouselEl?.querySelectorAll(".owl-item .item")[e.item.index];
    if (currentItemEl) {
      const activeGalleryId = currentItemEl.getAttribute("data-product_gallery_id") as string;
      this.modalEl?.setAttribute("data-product_gallery_id", activeGalleryId);
      this.modalEl?.setAttribute("data-name", `Playset ${activeGalleryId}`);
    }
  }
}
