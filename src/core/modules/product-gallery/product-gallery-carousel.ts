import { ProductGalleryToggle } from "./product-gallery-toggle";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_left.png";
import ProductGallery from "./product-gallery";
import { TrackingUtils } from "../../utils/tracking";
import { ProductGalleryCarouselConfig } from "../../interfaces/product-gallery";

export const DEFAULT_PRODUCT_GALLERY_CAROUSEL_CONFIG: ProductGalleryCarouselConfig = {
  selector: "",
  loop: true,
  margin: 15,
  autoplay: true,
  autoplayTimeout: 5000,
  nav: true,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    768: {
      margin: 15,
      items: 3,
    },
    993: {
      margin: 81,
      items: 3,
    },
  },
};

export class ProductGalleryCarousel {
  config: ProductGalleryCarouselConfig;
  productGallery: ProductGallery;
  element!: HTMLElement | null;

  constructor(config: ProductGalleryCarouselConfig, productGallery: ProductGallery) {
    this.config = {
      ...DEFAULT_PRODUCT_GALLERY_CAROUSEL_CONFIG,
      ...config,
    };
    this.productGallery = productGallery;
    this.init();
  }

  init() {
    this.initElements();
  }

  initElements() {
    this.element = document.querySelector(this.config.selector);
    if (this.element) {
      this.initCarousel();
    }
  }

  initCarousel() {
    const config = {
      ...this.config,
      ...{
        onInitialized: this.onInitialized.bind(this),
        onChanged: this.onChanged.bind(this),
      },
    };
    $(this.element as HTMLDivElement).owlCarousel(config);
  }

  onInitialized() {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }

    this.productGallery.toggle = new ProductGalleryToggle(this.productGallery.config.toggle!, this.productGallery);

    if (this.productGallery.tracking) {
      const $carousel = $(this.element as HTMLDivElement);
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      $prev.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Left Click";
        // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
      });

      $next.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Right Click";
        // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
      });
    }
  }

  onChanged() {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }
  }
}
