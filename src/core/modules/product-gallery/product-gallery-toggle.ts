import ProductGallery from "./product-gallery";
import { ProductGalleryToggleConfig } from "../../interfaces/product-gallery";
import { TrackingUtils } from "../../utils/tracking";

export const DEFAULT_PRODUCT_GALLERY_TOGGLE_CONFIG: ProductGalleryToggleConfig = {
  selector: "",
  idAttr: "data-product_gallery_id",
};

export class ProductGalleryToggle {
  config;
  productGallery;
  elements!: NodeListOf<HTMLElement>;

  constructor(config: ProductGalleryToggleConfig, productGallery: ProductGallery) {
    this.config = {
      ...DEFAULT_PRODUCT_GALLERY_TOGGLE_CONFIG,
      ...config,
    };
    this.productGallery = productGallery;
    this.initElements();
    this.initEvents();

    if (this.config.onTrackingClick) {
      this.onTrackingClick = this.config.onTrackingClick.bind(this);
    }
  }

  initElements() {
    if (typeof this.config.selector === "string") {
      this.elements = document.querySelectorAll(this.config.selector);
    } else {
      this.elements = document.querySelectorAll(this.config.selector.join(", "));
    }
  }

  initEvents() {
    if (this.elements) {
      this.elements.forEach((toggle: HTMLElement) => {
        toggle.style.pointerEvents = "all";
        toggle.addEventListener("click", this.onToggleClick.bind(this));
      });
    }
  }

  onToggleClick(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    const target: HTMLElement = event.target as HTMLElement;
    const id = target.getAttribute(this.config.idAttr ?? "data-product_gallery_id") as string;
    const index = parseInt((target.getAttribute("data-index") || "0") as string);
    this.productGallery.modal.show(id, index);
    if (this.productGallery.tracking) {
      this.onTrackingClick(target);
    }
  }

  onTrackingClick(toggle: HTMLElement) {
    const name = toggle.getAttribute("data-name");
    const eventName = "Click";
    // TrackingUtils.SendGA(`${name} ${eventName}`);
  }
}
