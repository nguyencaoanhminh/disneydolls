import Microsite from "../..";
import { GalleryConfig } from "./../../interfaces/gallery";
import GalleryCarousel from "./gallery-carousel";
import GalleryCharacter from "./gallery-character";
import GalleryModal from "./gallery-modal";
import GalleryToggle from "./gallery-toggle";

const DEFAULT_GALLERY_CONFIG: GalleryConfig = {
  key: 'gallery',
  tracking: false,
};

export default class Gallery {
  key: string;
  tracking: boolean = false;
  config;
  microsite;
  character!: GalleryCharacter;
  modal!: GalleryModal;
  carousel!: GalleryCarousel;
  toggle!: GalleryToggle;
  
  constructor(config: GalleryConfig) {
    this.config = { ...DEFAULT_GALLERY_CONFIG, ...config };
    this.key = this.config.key;
    this.tracking = !!this.config.tracking;
    this.microsite = window.Microsite;
    this.init();
  }

  init() {
    if (this.config.modal) {
      this.modal = new GalleryModal(this.config.modal, this);
    }
    
    if (this.config.toggle && !this.config.carousel) {
      this.toggle = new GalleryToggle(this.config.toggle, this);
    }

    if (this.config.carousel) {
      this.carousel = new GalleryCarousel(this.config.carousel, this);
    }

    if (this.config.character && !this.config.modal) {
      this.character = new GalleryCharacter(this.config.character, this); 
    }
  }
}
