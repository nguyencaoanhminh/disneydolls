import { Gallery } from ".";
import { GalleryModalConfig } from "@core/interfaces/gallery";
import { TrackingUtils } from "@core/utils";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_right.png";

const DEFAULT_GALLERY_MODAL_CAROUSEL_CONFIG = {
  loop: true,
  items: 1,
  singleItem: true,
  margin: 30,
  nav: false,
  dots: false,
  mouseDrag: true,
  touchDrag: true,
  pullDrag: true,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
};

export const DEFAULT_GALLERY_MODAL_CONFIG = {
  selector: "",
  data: [],
  carousel: DEFAULT_GALLERY_MODAL_CAROUSEL_CONFIG,
};

export default class GalleryModal {
  gallery: Gallery;
  config: GalleryModalConfig;
  modalEl!: HTMLDivElement | null;
  btnCloseEl!: HTMLButtonElement | null;
  contentEl!: HTMLDivElement | null;
  carouselEl!: HTMLDivElement | null;
  activeGalleryId!: string;
  activeGalleryIndex!: number;

  constructor(config: GalleryModalConfig, gallery: Gallery) {
    this.config = config;
    this.gallery = gallery;
    this.initElements();
  }

  initElements() {
    this.modalEl = document.querySelector(this.config.selector);
    if (this.modalEl) {
      this.btnCloseEl = this.modalEl.querySelector(".gallery-modal-close");
      this.contentEl = this.modalEl.querySelector(".gallery-modal-body-inner");
      this.carouselEl = this.modalEl.querySelector(".gallery-modal-carousel");
      this.initEvents();
      this.initCarousel();
    }
  }

  initCarousel() {
    if (!this.carouselEl) return;
    const html = this.config.data
      .map((data, index) => {
        return `
        <div class="item gallery-modal-item" data-gallery_id="${data.id}" data-index="${index}">
          <div class="gallery-modal-item-inner">
            <div class="gallery-modal-left-col">
              <div class="gallery-modal-image">
                <div class="gallery-modal-image-main">
                  <img src="images/${data.id}/1.jpg" data-index="1" alt="Gallery ${data.id} - Main" />
                </div>
                <div class="gallery-modal-image-list">
                  <div class="gallery-modal-image-col">
                    <img src="images/${data.id}/1.jpg" data-index="1" class="gallery-modal-image-thumb active" data-name="1" alt="Gallery ${data.id} - 1" />
                  </div>
                  <div class="gallery-modal-image-col">
                    <img src="images/${data.id}/2.jpg" data-index="2" class="gallery-modal-image-thumb" data-name="2" alt="Gallery ${data.id} - 2" />
                  </div>
                  <div class="gallery-modal-image-col">
                    <img src="images/${data.id}/3.jpg" data-index="3" class="gallery-modal-image-thumb" data-name="3" alt="Gallery ${data.id} - 3" />
                  </div>
                  <div class="gallery-modal-image-col">
                    <img src="images/${data.id}/4.jpg" data-index="4" class="gallery-modal-image-thumb" data-name="4" alt="Gallery ${data.id} - 4" />
                  </div>
                </div>
              </div>
            </div>
            <div class="gallery-modal-right-col">
              <div class="gallery-modal-infor">
                <h3 class="gallery-modal-infor-title">${data.title}</h3>
                <p class="gallery-modal-infor-desc">${data.description}</p>
              </div>
            </div>
            
          </div>
        </div>
      `;
      })
      .join("\n");
    this.carouselEl.innerHTML = html;
    const config = {
      ...DEFAULT_GALLERY_MODAL_CAROUSEL_CONFIG,
      ...{
        onInitialized: this.onCarouselInitialized.bind(this),
        onChanged: this.onCarouselChanged.bind(this),
      },
      ...this.config.carousel,
    };
    $(this.carouselEl).owlCarousel(config);
  }

  initEvents() {
    this.btnCloseEl?.addEventListener("click", (e) => {
      this.hide();
      if (this.gallery.tracking) {
        const eventName = "Close";
        TrackingUtils.SendGA(`Product Popup Box ${this.activeGalleryId} ${eventName}`, 'Product Gallery', 'Engagement');
      }
    });
  }

  show(galleryId: string, index: any) {
    document.querySelectorAll("video").forEach((video) => {
      video.pause();
    });
    this.activeGalleryId = galleryId;
    // this.activeGalleryIndex = parseInt(this.carouselEl?.querySelector(`.gallery-modal-item[data-gallery_id="${galleryId}"]`)?.getAttribute("data-index") as string);
    this.updateModal();
    if (index) {
      $('html').css('overflow', 'hidden');
      $(this.carouselEl as HTMLDivElement).trigger("to.owl.carousel", parseInt(index));
      $(this.modalEl as HTMLDivElement).modal("show");
    }
  }

  hide() {
    $('html').css('overflow', 'auto');
    $(this.modalEl as HTMLDivElement).modal("hide");
  }

  updateModal() {
    this.modalEl?.setAttribute("data-gallery_id", this.activeGalleryId);
    if (this.gallery.tracking) {
      this.modalEl?.setAttribute("data-name", `Playset ${this.activeGalleryId}`);
    }
  }

  onCarouselInitialized() {
    const $carousel = $(this.carouselEl as HTMLDivElement);
    if (this.config.carousel?.nav && $($carousel.find(".owl-nav")).hasClass("disabled")) {
      $($carousel.find(".owl-nav")).removeClass("disabled");
    }

    const items: NodeListOf<HTMLElement> = this.carouselEl?.querySelectorAll(".item") as NodeListOf<HTMLElement>;
    items.forEach((item) => {
      const toggles: NodeListOf<HTMLImageElement> = item.querySelectorAll(".gallery-modal-image-col img");
      toggles.forEach((toggle: HTMLImageElement) => {
        toggle.addEventListener("click", (e: Event) => {
          const target = e.target as HTMLImageElement;
          const index = target.getAttribute("data-index") as string;
          const src = target.src as string;
          const main = item.querySelector(".gallery-modal-image-main img") as HTMLImageElement;
          main.src = src;
          main.setAttribute("data-index", index);
          toggles.forEach((ele) => ele.classList.remove("active"));
          target.classList.add("active");
        });
      });
    });

    if (this.gallery.tracking) {
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      $prev.on("click", (e) => {
        const eventName = "Left Click";
        TrackingUtils.SendGA(`Product Popup Box ${this.activeGalleryId} ${eventName}`, 'Product Gallery', 'Engagement');
      });

      $next.on("click", (e) => {
        const eventName = "Right Click";
        TrackingUtils.SendGA(`Product Popup Box ${this.activeGalleryId} ${eventName}`, 'Product Gallery', 'Engagement');
      });

      items.forEach((item) => {
        const toggles = item.querySelectorAll(".gallery-modal-image-col img");
        toggles.forEach((toggle) => {
          toggle.addEventListener("click", (e) => {
            const target: HTMLImageElement = e.target as HTMLImageElement;
            let idx_img = target.getAttribute("data-index")
            const eventName = "Clicked";
            TrackingUtils.SendGA(`Product ${this.activeGalleryId} Popup Box ${idx_img} ${eventName}`, 'Product Gallery', 'Engagement');
          });
        });
      });
    }
  }

  onCarouselChanged(e: any) {
    const $carousel = $(this.carouselEl as HTMLDivElement);
    if (this.config.carousel?.nav && $($carousel.find(".owl-nav")).hasClass("disabled")) {
      $($carousel.find(".owl-nav")).removeClass("disabled");
    }

    const currentItemEl = this.carouselEl?.querySelectorAll(".owl-item .item")[e.item.index];
    if (currentItemEl) {
      const activeGalleryId = currentItemEl.getAttribute("data-gallery_id") as string;
      // console.log("change", activeGalleryId)
      this.activeGalleryId = activeGalleryId;
      this.modalEl?.setAttribute("data-gallery_id", activeGalleryId);
      this.modalEl?.setAttribute("data-name", `Playset ${activeGalleryId}`);
    }
  }
}
