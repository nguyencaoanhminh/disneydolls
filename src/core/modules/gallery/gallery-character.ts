import { Gallery } from ".";
import { GalleryModalConfig } from "@core/interfaces/gallery";
import { TrackingUtils } from "@core/utils";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_right.png";

const DEFAULT_GALLERY_MODAL_CAROUSEL_CONFIG = {
  loop: true,
  items: 1,
  singleItem: true,
  margin: 30,
  nav: false,
  dots: false,
  mouseDrag: true,
  touchDrag: true,
  pullDrag: true,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
};

export const DEFAULT_GALLERY_MODAL_CONFIG = {
  selector: "",
  data: [],
  carousel: DEFAULT_GALLERY_MODAL_CAROUSEL_CONFIG,
};

export default class GalleryCharacter {
  gallery: Gallery;
  config: GalleryModalConfig;
  modalEl!: HTMLDivElement | null;
  btnCloseEl!: HTMLButtonElement | null;
  contentEl!: HTMLDivElement | null;
  carouselEl!: HTMLDivElement | null;
  activeGalleryId!: string;
  activeGalleryIndex!: number;

  constructor(config: GalleryModalConfig, gallery: Gallery) {
    this.config = config;
    this.gallery = gallery;
    this.initElements();
  }

  initElements() {
    this.modalEl = document.querySelector(this.config.selector);
    if (this.modalEl) {
      this.btnCloseEl = this.modalEl.querySelector(".gallery-character-close");
      this.contentEl = this.modalEl.querySelector(".gallery-character-body-inner");
      this.carouselEl = this.modalEl.querySelector(".gallery-character-carousel");
      this.initEvents();
      this.initCarousel();
    }
  }

  initCarousel() {
    if (!this.carouselEl) return;
    const htmlCharacter = this.config.data
      .map((data, index) => {
        return `
        <div class="item gallery-character-item" data-character_id="${data.id}" data-index="${index}">
          <div class="gallery-character-item-inner">
            <div class="gallery-character-left-col">
              <div class="gallery-character-image">
                <div class="gallery-character-image-main">
                  <img src="images/${data.id}/ava.jpg" data-index="1" alt="Gallery ${data.id} - Main" />
                </div>
              </div>
            </div>
            <div class="gallery-character-right-col">
              <div class="gallery-character-infor">
                <div class="group">
                  <img class="gallery-character-infor-title" src="images/${data.id}/name.png" data-index="1" alt="Gallery ${data.id} - Main" />
                  <p class="gallery-character-infor-desc">${data.description}</p>
                </div>
                <a><img src="images/quiz-btn.png" alt="Btn Play" /></a>
              </div>
            </div>            
          </div>
        </div>
      `;
      })
      .join("\n");
    this.carouselEl.innerHTML = htmlCharacter;

    const config = {
      ...DEFAULT_GALLERY_MODAL_CAROUSEL_CONFIG,
      ...{
        onInitialized: this.onCarouselInitialized.bind(this),
        onChanged: this.onCarouselChanged.bind(this),
      },
      ...this.config.carousel,
    };
    $(this.carouselEl).owlCarousel(config);
  }

  initEvents() {
    this.btnCloseEl?.addEventListener("click", (e) => {
      this.hide();
      if (this.gallery.tracking) {
        const modalName = this.modalEl?.getAttribute("data-name");
        const eventName = "Close Click";
        // TrackingUtils.SendGA(`${modalName} ${eventName}`);
      }
    });
  }

  show(galleryId: string, index: any) {
    document.querySelectorAll("video").forEach((video) => {
      video.pause();
    });
    this.activeGalleryId = galleryId;
    this.activeGalleryIndex = parseInt(this.carouselEl?.querySelector(`.gallery-character-item[data-character_id="${galleryId}"]`)?.getAttribute("data-index") as string);
    // console.log(this.activeGalleryId, this.activeGalleryIndex, this.carouselEl?.querySelector(`.gallery-character-item[data-character_id="${galleryId}"]`));
    this.updateModal();
    if (index) {
      $("html").css("overflow", "hidden");
      $(this.modalEl as HTMLDivElement).modal("show");
      $(this.carouselEl as HTMLDivElement).trigger("to.owl.carousel", parseInt(index));
    }
  }

  hide() {
    $("html").css("overflow", "auto");
    $(this.modalEl as HTMLDivElement).modal("hide");
  }

  updateModal() {
    this.modalEl?.setAttribute("data-character_id", this.activeGalleryId);
    if (this.gallery.tracking) {
      this.modalEl?.setAttribute("data-name", `Playset ${this.activeGalleryId}`);
    }
  }

  onCarouselInitialized() {
    const $carousel = $(this.carouselEl as HTMLDivElement);
    if (this.config.carousel?.nav && $($carousel.find(".owl-nav")).hasClass("disabled")) {
      $($carousel.find(".owl-nav")).removeClass("disabled");
    }

    const items: NodeListOf<HTMLElement> = this.carouselEl?.querySelectorAll(".item") as NodeListOf<HTMLElement>;
    items.forEach((item) => {
      const toggles: NodeListOf<HTMLImageElement> = item.querySelectorAll(".gallery-character-image-col img");
      toggles.forEach((toggle: HTMLImageElement) => {
        toggle.addEventListener("click", (e: Event) => {
          const target = e.target as HTMLImageElement;
          const index = target.getAttribute("data-index") as string;
          const src = target.src as string;
          const main = item.querySelector(".gallery-character-image-main img") as HTMLImageElement;
          main.src = src;
          main.setAttribute("data-index", index);
          toggles.forEach((ele) => ele.classList.remove("active"));
          target.classList.add("active");
        });
      });
    });

    if (this.gallery.tracking) {
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      $prev.on("click", (e) => {
        const eventName = "Left Click";
        TrackingUtils.SendGA("Left Button Click", "Character Gallery", "Engagement");
      });

      $next.on("click", (e) => {
        const eventName = "Right Click";
        TrackingUtils.SendGA("Right Button Click", "Character Gallery", "Engagement");
      });

      items.forEach((item) => {
        const toggles = item.querySelectorAll(".gallery-character-image-col img");
        toggles.forEach((toggle) => {
          toggle.addEventListener("click", (e) => {
            const target: HTMLImageElement = e.target as HTMLImageElement;
            const modalName = this.modalEl?.getAttribute("data-name");
            const carouselName = this.carouselEl?.getAttribute("data-name");
            const boxName = target.getAttribute("data-name");
            const eventName = "Click";
            // TrackingUtils.SendGA(`${modalName} ${carouselName} ${boxName} ${eventName}`);
          });
        });
      });
    }
  }

  onCarouselChanged(e: any) {
    const $carousel = $(this.carouselEl as HTMLDivElement);
    if (this.config.carousel?.nav && $($carousel.find(".owl-nav")).hasClass("disabled")) {
      $($carousel.find(".owl-nav")).removeClass("disabled");
    }

    const currentItemEl = this.carouselEl?.querySelectorAll(".owl-item .item")[e.item.index];
    if (currentItemEl) {
      const activeGalleryId = currentItemEl.getAttribute("data-character_id") as string;
      this.modalEl?.setAttribute("data-character_id", activeGalleryId);
      this.modalEl?.setAttribute("data-name", `Playset ${activeGalleryId}`);
    }
  }
}
