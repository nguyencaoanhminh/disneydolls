import { Gallery } from ".";
import { GalleryToggleConfig } from "@core/interfaces/gallery";
import { TrackingUtils } from "@core/utils";

export const DEFAULT_GALLERY_TOGGLE_CONFIG: GalleryToggleConfig = {
  selector: "",
};

export default class GalleryToggle {
  config;
  gallery;
  elements!: NodeListOf<HTMLElement>;

  constructor(config: GalleryToggleConfig, gallery: Gallery) {
    this.config = {
      ...DEFAULT_GALLERY_TOGGLE_CONFIG,
      ...config,
    };
    this.gallery = gallery;
    this.initElements();
    this.initEvents();

    if (this.config.onTrackingClick) {
      this.onTrackingClick = this.config.onTrackingClick.bind(this);
    }
  }

  initElements() {
    if (typeof this.config.selector === "string") {
      this.elements = document.querySelectorAll(this.config.selector);
    } else {
      this.elements = document.querySelectorAll(this.config.selector.join(", "));
    }
  }

  initEvents() {
    if (this.elements && window.innerWidth > 1200) {
      const slider: any = document.querySelector('.section-meet-list');
      let isMoved = false;
      let isDown = false;
      let startX: any, scrollLeft: any;
      let temp: any = 0;

      let startDragging = function (e: any) {
        let toggle = document.querySelectorAll(".character-gallery-toggle");
        toggle.forEach((e: any) => {
          e.setAttribute("data-clickable", "false");
        })
        isDown = true;
        isMoved = false;
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
        temp = slider.scrollLeft;
      };
      let stopDragging = function (event: any) {
        event.preventDefault();
        isDown = false;
      };

      slider.addEventListener('mousemove', (e: any) => {
        if(!isDown) return;
        if (Math.abs(temp - slider.scrollLeft) > 30) {
          isMoved = true;
          $(".section-meet-list-container-arrow").addClass("hide");
        }
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const scroll = x - startX;
        slider.scrollLeft = scrollLeft - scroll;
      });
      
      // Add the event listeners
      slider.addEventListener('mousedown', startDragging);
      slider.addEventListener('mouseup', stopDragging);
      slider.addEventListener('mouseleave', stopDragging);
      for (var i = 0; i < this.elements.length; i++) {
        this.elements[i].addEventListener('click', function(e) {
          if (!isMoved) {
            this.setAttribute("data-clickable", "true");
          }
        });
      }
      this.elements.forEach((toggle: any) => {
        toggle.addEventListener("click", this.onToggleClick.bind(this));
      });
      $('#products-carousel').on('click', '.section-products-carousel-item', this.onToggleClick.bind(this));
    } else if (this.elements && window.innerWidth <= 1200) {
      const slider: any = document.querySelector('.section-meet-list');
      let isMoved = false;
      let isDown = false;
      let startX: any, scrollLeft: any;
      let temp: any = 0;

      let startDragging = function (e: any) {
        let toggle = document.querySelectorAll(".character-gallery-toggle");
        toggle.forEach((e: any) => {
          e.setAttribute("data-clickable", "false");
        });
        // console.log('e', e);
        // console.log('pageX', e.changedTouches[0].pageX);
        // console.log('slider', slider.scrollLeft);
        isDown = true;
        isMoved = false;
        startX = e.changedTouches[0].pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
        temp = slider.scrollLeft;
      };
      let stopDragging = function (event: any) {
        isDown = false;
      };

      slider.addEventListener('touchmove', (e: any) => {
        if(!isDown) return;
        // console.log(Math.abs(temp - slider.scrollLeft));
        if (Math.abs(temp - slider.scrollLeft) > 30) {
          isMoved = true;
          $(".section-meet-list-container-arrow").addClass("hide");
        }
        e.preventDefault();
        const x = e.changedTouches[0].pageX - slider.offsetLeft;
        const scroll = x - startX;
        slider.scrollLeft = scrollLeft - scroll;
      });
      
      // Add the event listeners
      slider.addEventListener('touchstart', startDragging);
      slider.addEventListener('touchend', stopDragging);
      slider.addEventListener('touchcancel', stopDragging);
      for (var i = 0; i < this.elements.length; i++) {
        this.elements[i].addEventListener('click', function(e) {
          if (!isMoved) {
            this.setAttribute("data-clickable", "true");
          }
        });
      }
      this.elements.forEach((toggle: any) => {
        toggle.addEventListener("click", this.onToggleClick.bind(this));
      });
      $('#products-carousel').on('click', '.section-products-carousel-item', this.onToggleClick.bind(this));
    }
  }

  onToggleClick(event: any) {
    event.preventDefault();
    event.stopPropagation();
    const target: HTMLElement = event.target as HTMLElement;
    let layerY = event?.layerY;
    if (this.config.selector == ".product-gallery-toggle") {
      const id = target.getAttribute("data-gallery_id") as string;
      const index = target.getAttribute("data-index") as string;
      id && this.gallery.modal.show(id, index);

      if (this.gallery.tracking ) {
        let content_height = window.innerWidth > 575 ? 313 : (window.innerWidth * 47 / 100);
        if (layerY > content_height) {
          this.onTrackingClick(target, true);
        } else {
          this.onTrackingClick(target, false);
        }
      }
    }
    if (this.config.selector == ".character-gallery-toggle") {
      const id_c = target.getAttribute("data-character_id") as string;
      const index = target.getAttribute("data-index") as string;
      const clickable = target.getAttribute("data-clickable") as string;
      id_c && clickable == "true" && this.gallery.character.show(id_c, index);

      if (this.gallery.tracking && clickable == "true") {
        this.onTrackingClickCharacter(target);
      }
    }
  }

  onTrackingClick(toggle: HTMLElement, isSeeMore: boolean) {
    const id = toggle.getAttribute("data-gallery_id");
    if (isSeeMore) {
      // TrackingUtils.SendGA(`Product ${id} Clicked`, "Product Gallery", "Engagement");
      TrackingUtils.SendGA(`Product ${id} 'See More' Clicked`, "Product Gallery", "Engagement");
    } else {
      TrackingUtils.SendGA(`Product ${id} Clicked`, "Product Gallery", "Engagement");
      // TrackingUtils.SendGA(`Product ${id} Image Clicked`, "Product Gallery", "Engagement");
    }
  }
  onTrackingClickCharacter(toggle: HTMLElement) {
    const name = toggle.getAttribute("data-name");
    TrackingUtils.SendGA(`Character ${name} Click`, "Character Gallery", "Engagement");
  }
}
