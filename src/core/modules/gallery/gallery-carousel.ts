import { GalleryCarouselConfig } from "./../../interfaces/gallery";
import angleLeft from "../../../assets/images/angle_left.png";
import angleRight from "../../../assets/images/angle_right.png";
import { Gallery, GalleryToggle } from ".";
import { LayoutUtils, TrackingUtils } from "@core/utils";

export const DEFAULT_GALLERY_CAROUSEL_CONFIG: GalleryCarouselConfig = {
  selector: "",
  loop: true,
  margin: 15,
  autoplay: true,
  autoplayTimeout: 5000,
  nav: true,
  navText: [`<img src="${angleLeft}" alt="" />`, `<img src="${angleRight}" alt="" />`],
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    768: {
      margin: 15,
      items: 3,
    },
    993: {
      margin: 81,
      items: 3,
    },
  },
};

export default class GalleryCarousel {
  config: GalleryCarouselConfig;
  gallery: Gallery;
  element!: HTMLElement | null;

  constructor(config: GalleryCarouselConfig, gallery: Gallery) {
    this.config = {
      ...DEFAULT_GALLERY_CAROUSEL_CONFIG,
      ...config,
    };
    this.gallery = gallery;
    this.init();
  }

  init() {
    this.initElements();
  }

  initElements() {
    this.element = document.querySelector(this.config.selector);
    if (this.element) {
      this.initCarousel();
    }
  }

  initCarousel() {
    const config = {
      ...this.config,
      ...{
        onInitialized: this.onInitialized.bind(this),
        onChanged: this.onChanged.bind(this)
      },
    };
    LayoutUtils.windowOnLoad(() => {
      $(this.element as HTMLDivElement).owlCarousel(config);
    });
  }

  onInitialized() {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }

    this.gallery.toggle = new GalleryToggle(this.gallery.config.toggle!, this.gallery);

    if (this.gallery.tracking) {
      const $carousel = $(this.element as HTMLDivElement);
      const $prev = $($carousel.find(".owl-nav .owl-prev"));
      const $next = $($carousel.find(".owl-nav .owl-next"));
      $prev.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Left Click";
        // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
      });

      $next.on("click", (e) => {
        const carouselName = $carousel.data("name") ?? "Unknown";
        const eventName = "Right Click";
        // TrackingUtils.SendGA(`${carouselName} ${eventName}`);
      });
    }
  }

  onChanged() {
    if (this.config.nav && $(this.element as HTMLDivElement).find(".owl-nav.disabled").length > 0) {
      $(this.element as HTMLDivElement)
        .find(".owl-nav")
        .removeClass("disabled");
    }
  }
}
