import { VideoControlOptions } from "./../../interfaces/video";
import Video from "./video";

export abstract class VideoControl {
  button: HTMLButtonElement;
  parent: Video;

  abstract onClick(event: Event): void;

  constructor(parent: Video, options: VideoControlOptions) {
    this.parent = parent;
    this.button = document.createElement("BUTTON") as HTMLButtonElement;
    this.button.type = "button";
    this.button.className = `video-control ${options.type} ${
      (options.type == "play" && this.parent.video.paused) || (options.type == "soundon" && !this.parent.video.muted) || (options.type == "soundoff" && this.parent.video.muted) ? "show" : ""
    }`;
    this.button.innerHTML = `<img src="${options.image}" alt=""/>`;
    this.parent.container.appendChild(this.button);

    const events = options.events || ["click"];
    events.forEach((event) => {
      this.button.addEventListener(event, this.onClick.bind(this));
    });
  }

  hide() {
    this.button.classList.remove("show");
  }

  show() {
    this.button.classList.add("show");
  }
}

export class PlayControl extends VideoControl {
  constructor(parent: Video, options: VideoControlOptions) {
    super(parent, { ...options, type: "play" });
  }

  onClick(event: Event): void {
    this.parent.video.play();
  }
}

export class ReplayControl extends VideoControl {
  constructor(parent: Video, options: VideoControlOptions) {
    super(parent, { ...options, type: "replay" });
  }

  onClick(event: Event): void {
    this.parent.video.play();
  }
}

export class SoundOnControl extends VideoControl {
  constructor(parent: Video, options: VideoControlOptions) {
    super(parent, { ...options, type: "soundon" });
  }

  onClick(event: Event): void {
    this.parent.video.muted = true;
  }
}

export class SoundOffControl extends VideoControl {
  constructor(parent: Video, options: VideoControlOptions) {
    super(parent, { ...options, type: "soundoff" });
  }

  onClick(event: Event): void {
    this.parent.video.muted = false;
  }
}
