import { PlayControl, ReplayControl, SoundOffControl, SoundOnControl } from "./controls";
import defaultPlayImage from "../../../assets/images/btn_play.png";
import defaultReplayImage from "../../../assets/images/btn_replay.png";
import defaultSoundOnImage from "../../../assets/images/btn_sound_on.png";
import defaultSoundOffImage from "../../../assets/images/btn_sound_off.png";
import { VideoConfig } from "./../../interfaces/video";
import { TrackingUtils } from "@core/utils";

const DEFAULT_VIDEO_CONFIG: VideoConfig = {
  key: "default",
  selector: "",
  controls: true,
};

export default class Video {
  microsite: any;
  config: VideoConfig;
  video!: HTMLVideoElement;
  container!: HTMLElement;
  btnPlay!: PlayControl;
  btnReplay!: ReplayControl;
  btnSoundOff!: SoundOffControl;
  btnSoundOn!: SoundOnControl;

  get isEnableAllControl(): boolean {
    return this.config.controls === true;
  }

  get isEnablePlayControl(): boolean {
    return this.isEnableAllControl || (typeof this.config.controls == "object" && !!this.config.controls.play);
  }

  get isEnableReplayControl(): boolean {
    return this.isEnableAllControl || (typeof this.config.controls == "object" && !!this.config.controls.replay);
  }

  get isEnableSoundControl(): boolean {
    return this.isEnableAllControl || (typeof this.config.controls == "object" && !!this.config.controls.sound);
  }

  get playControlImage(): any {
    if (typeof this.config.controls == "object") {
    }
    return defaultPlayImage;
  }

  get replayControlImage(): any {
    return defaultReplayImage;
  }

  get soundOnControlImage(): any {
    return defaultSoundOnImage;
  }

  get soundOffControlImage(): any {
    return defaultSoundOffImage;
  }

  constructor(config = {}) {
    this.microsite = window.Microsite;
    this.config = { ...DEFAULT_VIDEO_CONFIG, ...config };
    this.init();
  }

  init() {
    this.initElements();
    this.initControls();
    // this.initEvents();
  }

  initElements() {
    this.video = document.querySelector(this.config.selector) as HTMLVideoElement;
    if (this.video) {
      this.video.setAttribute("data-ended", "false");
      this.container = this.video.parentElement || document.body;
    }
  }

  initControls() {
    if (this.video && this.config.controls) {
      this.video.controls = false;
      if (this.isEnablePlayControl) {
        this.btnPlay = new PlayControl(this, { image: this.playControlImage });
      }

      if (this.isEnableReplayControl) {
        this.btnReplay = new ReplayControl(this, { image: this.replayControlImage });
      }

      if (this.isEnableSoundControl) {
        this.btnSoundOn = new SoundOnControl(this, { image: this.soundOnControlImage });
        this.btnSoundOff = new SoundOffControl(this, { image: this.soundOffControlImage });
      }
    }
  }

  initEvents() {
    this.video.addEventListener("play", this.onPlay.bind(this));
    this.video.addEventListener("pause", this.onPause.bind(this));
    this.video.addEventListener("ended", this.onEnded.bind(this));
    this.video.addEventListener("volumechange", this.onVolumeChange.bind(this));
    this.video.addEventListener("timeupdate", this.onTimeUpdate.bind(this));

    const supportsTouch = "ontouchstart" in window || navigator.maxTouchPoints || navigator.msMaxTouchPoints;
    if (supportsTouch) {
      this.video.addEventListener("touchend", this.onClickVideo.bind(this));
    } else {
      this.video.addEventListener("click", this.onClickVideo.bind(this));
    }
  }

  onClickVideo(event: any) {
    if (!this.video.paused) {
      event.preventDefault();
      event.stopPropagation();
      this.video.pause();
    } else {
      event.preventDefault();
      event.stopPropagation();
      this.video.play();
    }
  }

  onPlay(event: any) {
    if (this.video.getAttribute("data-ended") == "true") {
      this.onReplay();
      setTimeout(() => {
        this.video.setAttribute("data-ended", "false");
      }, 100);
    } else {
      if (this.config.tracking) {
        const videoName: any = this.video.getAttribute("data-name");
        const eventName = "Play Click";
        // TrackingUtils.SendGA(`${videoName} ${eventName}`);
        TrackingUtils.SendGA("Video Play", videoName, "Video Interaction");
      }
    }

    if (this.isEnablePlayControl) {
      this.video.setAttribute("controls", "true");
      this.btnPlay.hide();
    }

    document.querySelectorAll("video").forEach((video) => {
      if (video != this.video) {
        video.pause();
      }
    });
  }

  onPause(event: any) {
    if (this.isEnablePlayControl) {
      this.video.removeAttribute("controls");
      this.btnPlay.show();
    }

    if (this.config.tracking) {
      const videoName: any = this.video.getAttribute("data-name");
      const isTV: any = this.video.getAttribute("data-tv");
      const eventName = "Pause Click";
      if (isTV && this.video.paused) {
        setTimeout(() => {
          if (this.video.getAttribute("data-ended") == "false") {
            TrackingUtils.SendGA("Video Pause", videoName, "Video Interaction");
          }
        }, 150);
      } else {
        setTimeout(() => {
          if (this.video.getAttribute("data-ended") == "false") {
            TrackingUtils.SendGA("TAS 30 Sec Video Pause_Clicked", videoName, "Video Interaction");
          }
        }, 150);
      }
    }
  }

  onEnded(event: any) {
    this.video.setAttribute("data-ended", "true");
    if (this.isEnableReplayControl) {
      this.video.removeAttribute("controls");
      this.btnPlay.hide();
      this.btnReplay.show();
    } else if (this.isEnablePlayControl) {
      this.video.removeAttribute("controls");
      this.btnPlay.show();
    }
  }

  onReplay() {
    if (this.isEnableReplayControl) {
      this.btnReplay.hide();
    }

    if (this.config.tracking) {
      if (!this.video.paused) {
        const videoName: any = this.video.getAttribute("data-name");
        const eventName = "Replay Click";
        // TrackingUtils.SendGA(`${videoName} ${eventName}`);
        TrackingUtils.SendGA("Video Replay", videoName, "Video Interaction");
      }
    }
  }

  onVolumeChange() {
    if (this.isEnableSoundControl) {
      if (this.video.muted) {
        this.btnSoundOff.show();
        this.btnSoundOn.hide();
      } else {
        this.btnSoundOff.hide();
        this.btnSoundOn.show();
      }
    }

    if (this.config.tracking) {
      const videoName: any = this.video.getAttribute("data-name");
      const isTV: any = this.video.getAttribute("data-tv");
      if (isTV) {
        if (this.video.muted) {
          const eventName = "PlaySoundOff Click";
          // TrackingUtils.SendGA(`${videoName} ${eventName}`);
          TrackingUtils.SendGA("Video Sound Off", videoName, "Video Interaction");
          this.video.setAttribute("data-soundoff", "true");
        } else {
          if (this.video.getAttribute("data-soundoff")) {
            const eventName = "PlaySoundOn Click";
            // TrackingUtils.SendGA(`${videoName} ${eventName}`);
            TrackingUtils.SendGA("Video Sound on", videoName, "Video Interaction");
            this.video.removeAttribute("data-soundoff");
          }
        }
      } else {
        if (this.video.muted) {
          const eventName = "PlaySoundOff Click";
          // TrackingUtils.SendGA(`${videoName} ${eventName}`);
          TrackingUtils.SendGA("TAS 30 Sec Video PlaySoundOff_Clicked", videoName, "Video Interaction");
          this.video.setAttribute("data-soundoff", "true");
        } else {
          if (this.video.getAttribute("data-soundoff")) {
            const eventName = "PlaySoundOn Click";
            // TrackingUtils.SendGA(`${videoName} ${eventName}`);
            TrackingUtils.SendGA("TAS 30 Sec Video PlaySoundOn_Clicked", videoName, "Video Interaction");
            this.video.removeAttribute("data-soundoff");
          }
        }
      }
    }
  }

  onTimeUpdate(event: any) {
    if (this.config.tracking) {
      event.stopPropagation();
      const $video = $(this.video);
      const videoName = $video.data("name");
      // const watchedTime = Tracking.GetPlayedTime(e.target.played) * 100;
      const watchedTime = $video[0].currentTime * 100;
      const duration = $video[0].duration;
      if (Math.floor(watchedTime / duration) >= 25 && (!!$video.attr("data-25") == false || $video.attr("data-25") == "false")) {
        $video.attr("data-25", "true");
        const eventName = "Video played to 25%";
        // TrackingUtils.SendGA(`${videoName} ${eventName}`, "info");
        TrackingUtils.SendGA(eventName, videoName, "Video Interaction");
      }
      if (Math.floor(watchedTime / duration) >= 50 && (!!$video.attr("data-50") == false || $video.attr("data-50") == "false")) {
        $video.attr("data-50", "true");
        const eventName = "Video played to 50%";
        // TrackingUtils.SendGA(`${videoName} ${eventName}`, "info");
        TrackingUtils.SendGA(eventName, videoName, "Video Interaction");
      }
      if (Math.floor(watchedTime / duration) >= 75 && (!!$video.attr("data-75") == false || $video.attr("data-75") == "false")) {
        $video.attr("data-75", "true");
        const eventName = "Video played to 75%";
        // TrackingUtils.SendGA(`${videoName} ${eventName}`, "info");
        TrackingUtils.SendGA(eventName, videoName, "Video Interaction");
      }
      if (Math.floor(watchedTime / duration) >= 100 && (!!$video.attr("data-100") == false || $video.attr("data-100") == "false")) {
        $video.attr("data-100", "true");
        const eventName = "Video played to 100%";
        // TrackingUtils.SendGA(`${videoName} ${eventName}`, "info");
        TrackingUtils.SendGA(eventName, videoName, "Video Interaction");
      }
    }
  }
}
