export interface LayoutConfig {
  loader?: boolean | LoaderConfig,
  showInView?: boolean | ShowInViewConfig,
  autoScrollTop?: boolean,
  autoStopVideo?: false,
}

export interface LoaderConfig {
  selector: string,
  timeout: number
}

export interface ShowInViewConfig {
  selector: string
}