import { DefaultConfig } from './common';

export interface PlaylistConfig extends DefaultConfig {
  carousel: PlaylistCarouselConfig  
}

export interface PlaylistCarouselConfig extends OwlCarousel.Options {
  selector: string;
}