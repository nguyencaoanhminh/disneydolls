import { DefaultConfig } from './common';

export interface ProductGalleryConfig extends DefaultConfig {
  carousel?: ProductGalleryCarouselConfig;
  modal?: ProductGalleryModalConfig;
  toggle?: ProductGalleryToggleConfig;
}

export interface ProductGalleryToggleConfig {
  selector: string | Array<string>;
  idAttr?: string;
  onTrackingClick?(toggle: HTMLElement): void;
}

export interface ProductGalleryCarouselConfig extends OwlCarousel.Options {
  selector: string;
}

export interface ProductGalleryModalConfig {
  selector: string;
  data: Array<{
    id: string,
    title: string,
    image: string,
    description: string
  }>;
  carousel?: OwlCarousel.Options;
}
