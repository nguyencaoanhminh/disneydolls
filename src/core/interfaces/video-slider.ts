import { VideoConfig } from './video';
import { DefaultConfig } from './common';

export interface VideoSliderConfig extends DefaultConfig {
  carousel?: VideoSliderCarouselConfig,
  video?: VideoSliderVideoConfig
}

export interface VideoSliderCarouselConfig extends OwlCarousel.Options {
  selector: string;
  itemSelector?: string;
  titleSelector?: string;
}

export interface VideoCompletionPercent {
  percent: number,
  status: string | boolean,
}
export type VideoCompletion =  Array<VideoCompletionPercent>

export interface VideoSliderVideoConfig {
  selector: string;
}