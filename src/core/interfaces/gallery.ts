import { DefaultConfig } from "./common";

export interface WithGallery<T> {
  gallery: T;
}

export interface GalleryConfig extends DefaultConfig {
  carousel?: GalleryCarouselConfig;
  modal?: GalleryModalConfig;
  toggle?: GalleryToggleConfig;
  character?: GalleryCharacterConfig;
}

export interface GalleryToggleConfig {
  selector: string | Array<string>;
  onTrackingClick?(toggle: HTMLElement): void;
}

export interface GalleryCarouselConfig extends OwlCarousel.Options {
  selector: string;
}

export interface GalleryModalConfig {
  selector: string;
  data: Array<GalleryModalData>;
  carousel?: OwlCarousel.Options;
}

export interface GalleryCharacterConfig {
  selector: string;
  data: Array<GalleryModalData>;
  carousel?: OwlCarousel.Options;
}

export interface GalleryModalData {
  id: number,
  title: string,
  description: string,
  // descriptionMobile: string,
  // images?: Array<string>
}
