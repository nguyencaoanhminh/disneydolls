export interface DefaultConfig {
  key: string;
  tracking?: boolean;
}
