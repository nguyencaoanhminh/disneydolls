import { DefaultConfig } from "./common";

export interface VideoConfig extends DefaultConfig {
  selector: string;
  controls?: boolean | VideoControlConfig;
  controlEvents?: Array<string>;
}

export interface VideoControlConfig {
  play?: boolean;
  replay?: boolean;
  sound?: boolean;
}

export interface VideoControlOptions {
  image: string;
  type?: "play" | "replay" | "soundon" | "soundoff";
  events?: Array<string>;
}
