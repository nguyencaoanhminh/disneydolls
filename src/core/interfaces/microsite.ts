import { ProductGalleryConfig } from './product-gallery';
import { LayoutConfig } from './layout';
import { VideoSliderConfig } from './video-slider';
import { VideoConfig } from './video';
import { PlaylistConfig } from './playlist';
import { GalleryConfig } from './gallery';
export interface MicrositeConfig {
  gallery?: GalleryConfig | Array<GalleryConfig>,
  productGallery?: ProductGalleryConfig | Array<ProductGalleryConfig>,
  playlist?: PlaylistConfig | Array<PlaylistConfig>,
  video?: VideoConfig | Array<VideoConfig>,
  videoSlider?: VideoSliderConfig | Array<VideoSliderConfig>,
  layout?: LayoutConfig
}