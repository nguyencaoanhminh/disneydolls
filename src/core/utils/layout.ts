export class LayoutUtils {
  static windowOnLoad(callback: () => void, timeout = 1000) {
    setTimeout(() => {
      callback();
    }, timeout);
  }
}