export class TrackingUtils {
  static EventCategory: string = process.env.GA_EVENT_CATEGORY || "GA_EVENT_CATEGORY";
  // static SendGA(name: string, event?: string) {
  //   event = event || "Click";
  //   gtag("event", typeof event === "undefined" ? "Click" : event, {
  //     event_category: TrackingUtils.EventCategory,
  //     event_label: name,
  //   });
  //   console.log({ event: name, type: event });
  // }
  static SendGA(action: string, label: string, category: string) {
    gtag("event", action, {
      event_category: category,
      event_label: label,
    });
    console.log(action, { category: category, label: label });
  }
}
