import { Layout } from "./modules/layout";
import { Playlist } from "./modules/playlist";
import { MicrositeConfig } from "./interfaces/microsite";
import { Gallery } from "@core/modules/gallery";
import { Video } from "@core/modules/video";
import { VideoSlider } from "@core/modules/video-slider";
import { ProductGallery } from "@core/modules/product-gallery";

const DEFAULT_MICROSITE_CONFIG: MicrositeConfig = {};

class Microsite {
  config: MicrositeConfig;
  tvc: any;
  galleries!: Array<Gallery>;
  productGalleries!: Array<ProductGallery>;
  playlists!: Array<Playlist>;
  videos!: Array<Video>;
  videoSliders!: Array<VideoSlider>;
  layout!: Layout;

  constructor(config: MicrositeConfig = {}) {
    this.config = { ...DEFAULT_MICROSITE_CONFIG, ...config };
    this.initCore();
  }

  initCore() {
    this.initLayout();
    this.initVideos();
    this.initVideoSliders();
    this.initPlaylists();
    this.initGalleries();
    // this.initTVC();
    // this.initProductGalleries();

    // this.initVideoCarouselSliders();
    // this.initTracking();
  }

  initTVC() {
    // if (this.config.tvcConfig) {
    // }
  }

  initGalleries() {
    if (this.config.gallery) {
      if (this.config.gallery instanceof Array) {
        this.galleries = this.config.gallery.map((config) => new Gallery(config));
      } else {
        this.galleries = [new Gallery(this.config.gallery)];
      }
    }
  }

  initProductGalleries() {
    if (this.config.productGallery) {
      if (this.config.productGallery instanceof Array) {
        this.productGalleries = this.config.productGallery.map((config) => new ProductGallery(config));
      } else {
        this.productGalleries = [new ProductGallery(this.config.productGallery)];
      }
    }
  }

  initPlaylists() {
    if (this.config.playlist) {
      if (this.config.playlist instanceof Array) {
        this.playlists = this.config.playlist.map((config) => new Playlist(config));
      } else {
        this.playlists = [new Playlist(this.config.playlist)];
      }
    }
  }

  initVideos() {
    if (this.config.video) {
      if (this.config.video instanceof Array) {
        this.videos = this.config.video.map((config: any) => new Video(config));
      } else {
        this.videos = [new Video(this.config.video)];
      }
    }
  }

  initVideoSliders() {
    if (this.config.videoSlider) {
      if (this.config.videoSlider instanceof Array) {
        this.videoSliders = this.config.videoSlider.map((config: any) => new VideoSlider(config));
      } else {
        this.videoSliders = [new VideoSlider(this.config.videoSlider)];
      }
    }
  }

  // initVideoCarouselSliders() {
  //   if(this.config.videoCarouselSliderConfig) {
  //     this.videoCarouselSliders = this.config.videoCarouselSliderConfig.configs.map((config: any) => new VideoCarouselSlider(config, this)) ;
  //   }
  // }

  initLayout() {
    if (this.config.layout) {
      this.layout = new Layout(this.config.layout, this);
    }
  }

  // initTracking() {
  //   if(this.config.trackingConfig) {
  //     this.tracking = new Tracking(this.config.trackingConfig, this);
  //   }
  // }

  // getVideoCarouselSlider(key: any) {
  //   return this.videoCarouselSliders[this.videoCarouselSliders.findIndex((carousel: { config: { selector: any; }; }) => carousel.config.selector == key)];
  // }
}

export default Microsite;
