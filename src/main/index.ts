import Microsite from "../core";
import { LayoutUtils } from "@core/utils";
import ScrollTrigger from "gsap/ScrollTrigger";
import gsap from "gsap";
gsap.defaults({
  ease: "none",
});

gsap.registerPlugin(ScrollTrigger);
/**
 * Call methods in Modules folder
 */
class LEGOFriendsMicrosite extends Microsite {
  tl1!: any;
  config: any;
  constructor(config: any) {
    super(config);
    this.initHero();
    this.onAnimationEnd();
    // this.initScrollParallax();
  }

  onAnimationEnd() {
    $("#product-gallery-modal").on("hidden.bs.modal", function (e) {
      $("html").css("overflow", "auto");
    });
    $("#character-gallery-modal").on("hidden.bs.modal", function (e) {
      $("html").css("overflow", "auto");
    });
    let icon_text = $(".icon-hero");
    let quiz_btn = $(".section-quiz-button");
    let video_btn = $(".section-videos-button");
    let header_icon = document.querySelectorAll(".head-title-icon");
    icon_text.on("animationend", () => {
      icon_text.addClass("active");
    });
    quiz_btn.on("animationend", () => {
      quiz_btn.addClass("active");
    });
    video_btn.on("animationend", () => {
      video_btn.addClass("active");
    });
    header_icon.forEach((e: any) => {
      $(e).on("animationend", () => {
        $(e).addClass("active");
      });
    });
  }

  timeOutRemoveNone(item: any, time: number) {
    setTimeout(() => {
      item.removeClass("none");
    }, time);
  }

  initHero() {
    let wave = $(".section-hero-content-characters-wave");
    let text = $(".section-hero-content-text .image-main");
    let icon_text = $(".icon-hero");
    let nova = $(".name.nova");
    let olly = $(".name.olly");
    let aliya = $(".name.aliya");
    let paisley = $(".name.paisley");
    let autumn = $(".name.autumn");
    let leo = $(".name.leo");
    let liann = $(".name.liann");
    let zac = $(".name.zac");
    LayoutUtils.windowOnLoad(() => {
      wave.addClass("active");
      this.timeOutRemoveNone(text, 1500);
      this.timeOutRemoveNone(icon_text, 1800);
      this.timeOutRemoveNone(autumn, 400);
      this.timeOutRemoveNone(leo, 700);
      this.timeOutRemoveNone(liann, 1000);
      this.timeOutRemoveNone(zac, 1300);
      this.timeOutRemoveNone(nova, 1600);
      this.timeOutRemoveNone(olly, 1800);
      this.timeOutRemoveNone(aliya, 2000);
      this.timeOutRemoveNone(paisley, 2200);
    });
  }

  initScrollParallax() {
    const wd = window.innerWidth;
    const wh = window.innerHeight;
    this.tl1 = gsap.timeline({
      scrollTrigger: {
        trigger: ".section-quiz-container",
        start: `${wd > 576 ? `top ${wh > 1200 ? "+=65%" : wh > 770 ? "center" : wh > 690 ? "+=45%" : wh > 0 ? "+=90%" : ""}` : `top +=${wh / 2}px`}`,
        end: "bottom +=350px",
        pin: ".layout",

        scrub: true,
      },
    });
  }
}
export default LEGOFriendsMicrosite;
