/** 
 * Chỉ tham khảo cấu trúc của CONFIG
 */
const EXAMPLE_CONFIG = {
  videoConfig: {
    configs: [
      {
        selector: "#tvc-video",
        customSoundOn: false,
        customSoundOff: false,
      },
      {
        selector: "#sku-carousel-slider-video",
        customSoundOn: false,
        customSoundOff: false,
      },
      {
        selector: "#gallery-carousel-slider-video",
        customSoundOn: false,
        customSoundOff: false,
      },
    ],
  },
  designUnleashConfig: {
    selector: ".design-slider-container",
  },
  layoutConfig: {
    showInView: true,
    autoScrollTop: true,
    loader: true,
    autoStopVideo: {
      events: [
        (callback) => {
          $('.modal').modal('show.bs.modal', callback);
        }
      ] 
    }
  },
  productSliderConfig: {
    leftSelector: ".girl-before",
    rightSelector: ".pants-before",
  },
  footerCarouselConfig: {
    selector: ".owl-carousel-slide",
  },
  videoCarouselSliderConfig: {
    configs: [
      {
        selector: ".sku-carousel-slider-carousel",
        target: "#sku-carousel-slider-video",
        tracking: true,
      },
      {
        selector: ".gallery-carousel-slider-carousel",
        target: "#gallery-carousel-slider-video",
        targetTitle: "#gallery-carousel-slider-title",
        tracking: true,
      },
    ],
  },
  galleryConfig: {
    configs: [
      {
        modal: {
          selector: "#sliderGalleryModal",
          data: [
            {
              id: 41961,
              title: "ชุดเครื่องมือสำหรับออกแบบลวดลาย",
              description: "ปลดปล่อยความคิดสร้างสรรค์ด้วยชุดเครื่องมือสำหรับออกแบบ - ลวดลาย กับชุดเลโก้<span>&reg;</span> ดอทส์ ค้นพบชิ้นส่วนเลโก้หลากสีสัน พร้อมสนุกกับแผ่นผ้าใบเลโก้!",
            },
            {
              id: 41953,
              title: "กำไลข้อมือสายรุ้งพร้อมจี้ข้อมือ",
              description: "เพิ่มชิ้นส่วนเลโก้สีรุ้งแสนสนุก ติดลงบนฐานสีแดงสดใส มาพร้อมจี้หัวใจสีแดง โชว์สไตล์ที่ใช่ของคุณ!",
            },
            {
              id: 41956,
              title: "กรอบรูปไอศกรีม และกำไลข้อมือ",
              description: "มาทำให้ เลโก้<span>&reg;</span> ดอทส์ สนุกขึ้น! ด้วยการใส่กรอบรูปไอศกรีม และตกแต่งด้วยชิ้นส่วนเลโก้สีสันสดใส",
            },
            {
              id: 41954,
              title: "แผ่นติดแบบกาว",
              description: "แผ่นติดแบบกาว เลโก้<span>&reg;</span> ดอทส์ ขนาด 8x8 ติดชิ้นส่วนเลโก้หลากสีสันในรูปแบบที่แสดงตัวตนของคุณ แล้วนำแผ่นกาวไปติดบนพื้นผิวที่ต้องการ​ ",
            },
            {
              id: 41957,
              title: "แผ่นติดแบบกาวเมก้า แพ็ค",
              description: "อยู่กับสไตล์ของคุณได้ทุกที่ด้วยแผ่นติดแบบกาว เมก้า แพ็ค วางแผ่นติดที่มีกาวขนาด 8x8 ของ เลโก้<span>&reg;</span> ดอทส์ และเพิ่มชิ้นส่วนเลโก้สีสันสดใสในรูปแบบที่ชอบได้เลย",
            },
            {
              id: 41955,
              title: "แผ่นติดแบบเย็บ",
              description: "ออกแบบตัวตนของคุณในรูปแบบใหม่ๆ ด้วย แผ่นติดแบบเย็บ! วางตัวต่อ เลโก้<span>&reg;</span> ดอทส์ แบบยืดหยุ่น 6x6 แล้วติดชิ้นส่วนเลโก้ในแบบที่คุณชื่นชอบ​ได้เลย",
            },
            {
              id: 41960,
              title: "กล่องใหญ่",
              description: "เก็บของที่คุณชื่นชอบทั้งหมดไว้ใน เลโก้<span>&reg;</span> ดอทส์ กล่องใหญ่! ฝาปิดประกอบง่ายและรวดเร็ว พร้อมสนุกกับการตกแต่งได้ทันที​",
            },
            {
              id: 41959,
              title: "ถาดรูปแพนด้า",
              description: "สร้างเพื่อนที่น่ารักมาช่วยเก็บรักษาของรักของหวงของคุณ! เลโก้<span>&reg;</span> ดอทส์ ถาดรูปแพนด้าน่ารักๆ พร้อมสนุกกับตกแต่งด้วยชิ้นส่วนเลโก้",
            },
            {
              id: 41958,
              title: "รุ่น เอ็กซ์ตร้า ดอทส์ ซีรี่ส์ 7 – สปอร์ต ​​",
              description: "สร้างรูปแบบ การผสมผสานกับจินตนาการไม่สิ้นสุดด้วยชิ้นส่วนสีสันหลากหลาย พร้อมสีฟ้าสดใส พร้อมชิ้นส่วนตกแต่งสุดเซอร์ไพรส์อีกกว่า 10 ชิ้น!​",
            },
          ],
          tracking: true,
        },
        toggle: {
          selector: ".gallery-carousel-item",
          idAttr: "data-gallery_id",
        },
        carousel: {
          selector: ".gallery-carousel",
          carouselConfig: {
            navText: [`<img src="${angleLeftPurple}" alt="" />`, `<img src="${angleRightPurple}" alt="" />`],
          },
          tracking: true,
        },
      },
      {
        modal: {
          selector: "#productGalleryModal",
          data: [
            {
              id: 41947,
              title: "กำไลข้อมือเมก้า แพ็ค มิกกี้เมาส์ และผองเพื่อน",
              description: "เฉลิมฉลองไปกับมิกกี้เมาส์ ครอบครัว และผองเพื่อนจากดิสนีย์  ด้วยกำไลข้อมือ เมก้า แพ็คสุดเจ๋ง หยิบกำไล เลโก้<span>&reg;</span> ดอทส์ สีสันสดใส แล้วเลือกตกแต่งชิ้นส่วนเลโก้ได้ตามแบบที่ต้องการ",
            },
            {
              id: 41963,
              title: "กำไลข้อมือสติทช์ ออน แพ็ค มิกกี้เมาส์ และมินนี่เมาส์",
              description: "แสดงความรักของคุณที่มีต่อมิกกี้ เมาส์ และมินนี่ เมาส์ จากดิสนีย์ด้วย สติทช์ ออน แพ๊ค ชุดอุปกรณ์เย็บติดชิ้นนี้ วางตัวต่อ เลโก้<span>&reg;</span> ดอทส์ แบบไหนก็ได้ตามที่ต้องการ ในขนาด 6x6 แล้วติดบนสิ่งของเพื่อออกแบบตามที่ชื่นชอบ",
            },
            {
              id: 41964,
              title: "กล่องแบ็ค ทู สคูล มิกกี้เมาส์ และมินนี่เมาส์",
              description: "เปิดชุด เลโก้<span>&reg;</span> ดอทส์ และลองออกแบบดีไซน์ที่หลากหลาย ทั้งกรอบรูป ชุดลิ้นชัก ที่ใส่กระดาษโน้ต ป้ายติดกระเป๋า และแผ่นติดกาว!",
            },
          ],
          carouselConfig: {
            nav: false,
            dots: false,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false,
          },
          tracking: true,
        },
        toggle: {
          selector: ".section-product_item .content",
          idAttr: "data-gallery_id",
        },
      },
    ],
  },

  trackingConfig: {
    videoEvents: [
      {
        selector: "video",
        play: true,
      },
    ],
    clickEvents: [
      {
        selector: ".section-product_item .content",
      },
      {
        selector: ".gallery-carousel-item",
      },
    ],
  },
}