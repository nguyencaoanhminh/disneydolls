import { MicrositeConfig } from "./core/interfaces/microsite";
import angleLeftPurple from "./assets/images/angle_left_character.png";
import angleRightPurple from "./assets/images/angle_right_character.png";
import angleLeftVideo from "./assets/images/angle_left_video.png";
import angleRightVideo from "./assets/images/angle_right_video.png";
import { TrackingUtils } from "@core/utils/tracking";
export const LEGO_FRIENDS_MICROSITE_CONFIG: MicrositeConfig = {
  layout: {
    loader: true,
    showInView: true,
    autoScrollTop: true,
  },
  video: [
    {
      key: "tvc-video",
      selector: "#tvc-video",
      controls: {
        play: true,
        replay: true,
        sound: true,
      },
      tracking: true
    },
    {
      key: "adventure-video",
      selector: "#adventure-video",
      controls: {
        play: true,
        replay: true,
        sound: true,
      },
      tracking: true
    },
  ],
  videoSlider: {
    key: "tvc-video",
    carousel: {
      selector: "#tvc-video-carousel",
      margin: 0
    },
    video: {
      selector: "#tvc-video",
    },
    tracking: false,
  },
  playlist: [
    {
      key: "video-carousel",
      tracking: true,
      carousel: {
        selector: "#videos-carousel",
        navText: [`<img src="${angleLeftVideo}" alt="" />`, `<img src="${angleRightVideo}" alt="" />`],
        responsive: {
          0: {
            items: 2.3
          },
          1200: {
            items: 3.5
          }
        }
      },
    },
    {
      key: "meet-carousel",
      tracking: false,
      carousel: {
        selector: "#meet-carousel",
        responsive: {
          1200: {
            items: 4.7,
            autoWidth: true,
            margin: 0,
            smartSpeed: 500,
            animateIn: 'linear',
            animateOut: 'linear',
          }
        },
        nav: false,
      },
    },
    {
      key: "products-carousel",
      tracking: true,
      carousel: {
        selector: "#products-carousel",
        responsive: {
          0: {
            items: 2
          },
          1200: {
            items: 3
          }
        }
      },
    }
  ],
  gallery: [
    {
      key: "product-gallery-toggle",
      tracking: true,
      modal: {
        selector: "#product-gallery-modal",
        data: [
          {
            id: 41731,
            title: "Trường học quốc tế thành phố Heartlake 41731",
            description: "Chào mừng đến với Trường Học Quốc tế<br>thành phố Heartlake. Tiết đầu tiên là lớp<br>khoa học – môn học yêu thích của Autumn!<br>Giúp Aliya và Autumn chăm sóc thú cưng của<br>lớp, chú chuột hamster Bunsen.<br><br>Cùng Olly và Niko đi ăn trong căn tin hoặc<br>bước lên dãy cầu thang đầy màu sắc để dạo<br>chơi trên sân thượng. Đã đến tiết nghệ thuật<br>rồi! Khi các lớp học kết thúc, chạy ngay ra<br>sân chơi trượt ván hoặc chơi bóng rổ nào.",
          },
          {
            id: 41739,
            title: "Phòng ngủ của Liann 41739 ",
            description: "Cùng nhau ghé thăm phòng ngủ của Liann<br>nào! Liann yêu thích nghệ thuật và cô ấy rủ<br>Autumn đến để cùng vẽ tranh. Nhìn vào<br>phòng của Liann bạn sẽ thấy có rất nhiều<br>dụng cụ vẽ tranh và truyện tranh.<br><br>Sử dụng giá gỗ để vẽ bức tranh khắc hoạ<br>chú tắc kè của Liann- tên Popcorn. Vừa vẽ và<br>thưởng thức món ăn nhẹ bên ô cửa sổ.<br>Riêng Popcorn sẽ có thức ăn riêng đặc biệt.<br>Tìm trên giường của Liann - bịch thức ăn<br>được giấu sau kệ sách!",
          },
          {
            id: 41743,
            title: "Tiệm làm tóc thành phố Heartlake 41743",
            description: "Paisley lo lắng về tóc của mình. Cô ấy không<br>biết kiểu tóc nào sẽ phù hợp. May mắn thay,<br>bạn của cô ấy - Olly đã tới để giúp đỡ và đưa<br>lời khuyên. Anh ấy là một chuyên gia về thời<br>trang và sẵn lòng giúp Paisley tìm kiếm kiểu<br>tóc phù hợp.<br><br>Đây là Nadia, chủ tiệm làm tóc. Giúp cô ấy<br>tạo kiểu tóc mới và làm cho Paisley trở nên<br>lộng lẫy nào. Ghé vào máy bán kẹo sing gum<br>trên đường về nữa nhá!",
          },
          {
            id: 41728,
            title: "Nhà hàng trung tâm thành phố Heartlake 41728 ",
            description: "Đến ngay Nhà hàng trung tâm thành phố<br>Heartlake để gặp gỡ cô bạn Aliya nào! Cô ấy<br>đang đợi Liann. Chọn một bài hát thật chill<br>trên máy hát tự động hoặc chơi trò chơi trên<br>máy điện tử.<br><br>Đừng quên thử món bánh sandwich khổng<br>lồ nổi tiếng của Charli hoặc sử dụng các<br>nguyên liệu sẵn có để chế biến món ăn của<br>riêng bạn. Hãy thử trải nghiệm chỗ ngồi<br>ngoài trời nhưng cẩn thận chú mèo tinh<br>nghịch hay ghé thăm nhé!",
          },
          {
            id: 41730,
            title: "Ngôi nhà của Autumn 41730",
            description: "Cùng đến thăm nhà của Autumn. Hai người<br>bạn Leo và Aliya cũng đang ở đây để tham<br>gia hoạt động ngoài trời đầy thú vị. Giúp Leo<br>làm những chiếc bánh táo ngọt ngào. Đây là<br>mẹ của Autumn - cô Mia. Cùng Aliya chăm<br>sóc chú ngựa nhé, và thưởng thức món bánh<br>táo nướng ngon lành và latte vị bí ngô bên<br>lửa trại. Uh-oh, có vẻ như chú cún Daisy<br>đang quậy phá trong đống bí ngô - đã đến<br>lúc tắm cho nó rồi!",
          },
          {
            id: 41733,
            title: "Cửa hàng trà sữa di động 41733",
            description: "Sẵn sàng trải nghiệm món uống mới nhất với<br>Nova và Matilde? Hãy ghé ngay Cửa hàng<br>trà sữa di động ở trung tâm thành phố<br>Heartlake của người bạn Matilde. Giúp<br>Matilde pha chế những ly trà sữa trân châu<br>nổi tiếng và thưởng thức cùng người bạn<br>Nova.<br><br> Dùng dâu tây, kem sữa tươi và rắc thêm hạt<br>cốm để tạo ra các công thức mới thú vị. Bạn<br>muốn mang trà sữa trân châu đến với nhiều<br>khách hàng hơn nữa? Lái xe và sẵn sàng đi<br>giao hàng nào!",
          },
          {
            id: 41732,
            title: "Tiệm hoa và cửa hàng thiết kế nội thất 41732 ",
            description: "Thỏa sức sáng tạo và thiết kế  căn hộ LEGO<sup>®</sup><br>Friends theo cách riêng của bạn.  Adi và chú<br>cún Grace vừa chuyển đến Thành phố<br>Heartlake.<br><br>Giúp họ trang trí ngôi nhà với những món đồ<br>từ cửa hàng nội thất và hoa tươi để trang trí<br>thêm cho căn hộ.",
          },
          {
            id: 41727,
            title: "Trung tâm cứu hộ cún cưng 41727 ",
            description: "Autumn và bạn của cô ấy là Zac đang đến<br>thăm Tiến sĩ Gabriela tại Trung tâm Cứu hộ<br>cún cưng, nơi những chú chó Pickle, Dash và<br>Grace đang chờ được nhận nuôi. Bạn có thể<br>giúp chăm sóc chúng? Tắm rửa và chải lông<br>cho các bạn cún.<br><br>Và cùng giúp bác sĩ Gabriela kiểm tra sức<br>khỏe cho chúng nhé. Ồ ồ! Có vẻ như Dash<br>nghịch ngợm đã đào một cái lỗ! Chơi đùa với<br>những chú chó ở trung tâm thật là vui đúng<br>không?",
          },
        ],
        carousel: {
          nav: true,
          navText: [`<img src="${angleLeftPurple}" alt="" />`, `<img src="${angleRightPurple}" alt="" />`],
        },
      },
      toggle: {
        selector: ".product-gallery-toggle",
      },
    },
    {
      key: "character-gallery-toggle",
      tracking: true,
      character: {
        selector: "#character-gallery-modal",
        data: [
          {
            id: 1,
            title: "Paisley",
            description: "Kể cả khi hát, chơi guitar hoặc sáng tác bài hát,<br>âm nhạc là niềm vui thích của mình. Bạn mình<br>nói mình rất giỏi sáng tác. Nhưng mà mình<br>không giỏi trình diễn trên sân khấu. Olly đang<br>giúp mình tự tin hơn.",
          },
          {
            id: 2,
            title: "Autumn",
            description: "Bạn mình nói mình là một người nghịch ngợm,<br>thích khám phá và thân thiện với mọi người<br>(cả thú cưng nữa). Khi bạn thân Aliya ngủ ở<br>nhà mình, bọn mình hay tưởng tượng cùng<br>nhau cưỡi ngựa trong rừng - nhiều lúc mình<br>còn thuyết phục cô ấy đừng đi ngủ sớm!",
          },
          {
            id: 3,
            title: "Liann",
            description: "Bạn muốn đi quậy phá một chút không? Đi<br>cùng mình nhé! Mình thích vẽ vời, tô màu và<br>đọc truyện tranh. Khi cảm thấy chán nản, mình<br>sẽ cùng Zac đi trượt ván. Đôi khi tụi mình cũng<br>gặp rắc rối với những trò nghịch ngợm.. Oops!",
          },
          {
            id: 4,
            title: "Olly",
            description: "Bạn cần mình giúp cho Kênh Tin Tức Trường<br>Học? Không vấn đề. Cần mình thiết kế trang<br>phục cho lễ hội trường? Được luôn. Không biết<br>mặc gì và không nên mặc gì? Xem chương<br>trình của mình nhé. Hỏi Aliya hoặc bất kì ai<br>trong Thành Phố Heartlake, nếu bạn muốn<br>làm gì, mình có thể giúp!",
          },
          {
            id: 5,
            title: "Aliya",
            description: "Nếu bạn cần, bạn có thể mượn sổ tay của<br>mình! Autumn và Leo hay mượn để làm bài<br>tập cùng nhau. Khi mình được nghỉ, mình rất<br>thích tham gia cưỡi ngựa và bơi lội. Nếu bạn<br>giữ bí mật, mình sẽ nói chọ bạn biết nơi yêu<br>thích nhất của mình trên bãi biển.",
          },
          {
            id: 6,
            title: "Leo",
            description: "Thử miếng bánh vừa ra lò nhé? Mình vừa nấu<br>một số món ngon mới vì mình cần nạp năng<br>lượng cho trận bóng đá sắp tới! Mình từng rất<br>lo lắng khi chuyển đến Thành Phố Heartlake,<br>nhưng những người bạn mới như Zac khiến<br>mình vui vẻ hơn khi ở đây. Cùng xem phim nhé<br>mọi người? Mình sẽ mang đồ ăn ngon tới!"
          },
          {
            id: 7,
            title: "Zac",
            description: "Hey - Mình là Zac! Muốn xin chữ ký à? Không<br>hả? Được rồi! Mình có thể cho bạn xem vài<br>tuyệt chiêu võ thuật nếu bạn muốn, hoặc<br>xem hoạt hình hay là chơi game ở tiệm Diner<br>- Hey! Nova cũng thích chơi game đấy! Mình<br>rất tức mỗi khi cô ấy chiến thắng, nhưng<br>mình thích chơi game với cô ấy."
          },
          {
            id: 8,
            title: "Nova",
            description: "Game thủ, lập trình viên và chế tạo máy bay<br>điều khiển - mình đấy! Trên mạng mình là<br>Streamer, nhưng ngoài đời mình làm bạn với<br> chú cún tuyệt vời nhất thế gian, Pickles! Nó<br>không phải là bạn cún bình thường! Tụi mình<br>vô tình gặp gỡ tại trung tâm giải cứu động vật,<br>Autumn bạn mình là tình nguyện viên ở đó."
          }
        ],
        carousel: {
          nav: true,
          navText: [`<img src="${angleLeftPurple}" alt="" />`, `<img src="${angleRightPurple}" alt="" />`],
          smartSpeed: 500,
          onDragged: (e) => {
            const drag = e.relatedTarget['_drag']['direction'];
            if (drag == "left") {
              TrackingUtils.SendGA("Swipe Right", "Character Gallery", "Engagement");
            } else {
              TrackingUtils.SendGA("Swipe Left", "Character Gallery", "Engagement");
            }
          }
        },
      },
      toggle: {
        selector: ".character-gallery-toggle",
      },
    },
  ],
  // video: [
  //   {
  //     key: "example-video",
  //     selector: "#example-video",
  //     controls: true,
  //     tracking: true,
  //   },
  //   {
  //     key: "example-video",
  //     selector: "#example-video-slider-video",
  //     controls: true,
  //     tracking: true,
  //   },
  // ],
  // videoSlider: {
  //   tracking: true,
  //   key: "example-video-slider",
  //   carousel: {
  //     selector: ".video-carousel-slider-carousel",
  //   },
  //   video: {
  //     selector: "#example-video-slider-video",
  //   },
  // },
  // productGallery: {
  //   key: 'example-product-gallery',
  //   modal: {
  //     selector: "#example-product-gallery-modal",
  //     data: [
  //       {
  //         id: '1',
  //         title: 'TEST 1',
  //         description: 'TEST 1',
  //         image: '41953/1.png'
  //       },
  //       {
  //         id: '2',
  //         title: 'TEST 2',
  //         description: 'TEST 2',
  //         image: '41953/2.png'
  //       }
  //     ]
  //   },
  //   toggle: {
  //     selector: ['.example-product-gallery-toggle']
  //   }
  // }
};
