declare module "*.png";

function gtag(event: string, eventName: string, eventData: object): void;

declare interface Window {
  Microsite: Microsite;
}

declare interface Navigator {
  msMaxTouchPoints: number;
}
